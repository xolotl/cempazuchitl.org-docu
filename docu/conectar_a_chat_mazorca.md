Title: Iniciar sesion en chat.mazorca.org (mattermost)
Summary: Mattermost es una alternativa para organizar las comunicaciones de un equipo de trabajo, accesible desde computadoras o celulares. Es la opcion que elegimos para coordinar las actividades de mazorca.org
Slug:conectarse-al-chat-de-mazorca
Date: 2017-03-25
Category:mattermost
Tags: mattermost, chat, basicos
LANG: es

#Conectando a chat.mazorca.org

Conectar a chat.mazorca.org para enviarnos tus comentarios, sugerencias, peticiones de ayuda o soporte; te permitira mantener una conversacion organizada con toda la communidad de mazorca.org y a nosotros poder contestarte personalizadamente.

Una vez que hayas creado tu cuenta de chat **(LINK aqui)** deberas iniciar sesion, ve a https://chat.mazorca.org (no ponemos link por que es una buena practica escribir la direccion de una pagina de login).

![](../static/img/chat/login.png)

Una vez que haz tenido acceso tendras esta pantalla:

![](../static/img/chat/inicial.png)

Esta dividida en 4 secciones verticales, pegado a la izquierda tendras los equipos, aparecera segun los equipos en los cuales te encuentres.

![](../static/img/chat/equipos.png)

La siguiente seccion contiene los canales en los cuales se puede dividir el trabajo del equipo (aparecen al menos dos canales _Fuera de topico_ y _General_ en los cuales se puede charlar de cualquier tema con el equipo); en esta misma seccion mas abajo encontraras un espacio para _Grupos privados_ los cuales puedes crear para tener una conversacion con algunos miembros del equipo y _Mensajes Directos_ donde podras enviar mensajes solo al usuario que elijas. 

![](../static/img/chat/canales.png)

La tercera seccion es el espacio para los mensajes, hasta arriba tendras el nombre del canal, grupo o usuario a quien estes enviando los mensajes.

![](../static/img/chat/mensajes.png)

La ultima seccion te permitira cuatro acciones:

* Mostrar los integrantes del equipo y enviarles mensajes directos

![](../static/img/chat/miembros.png)

* Buscar mensajes usando **"comillas"** para buscar frases especificas, **from:** para buscar mensajes de un usuario especifico e **in** para buscar mensajes en canales especificos. Incluso puedes mezclar las tres opciones.

![](../static/img/chat/buscar.png)

* Con el simbolo **@** puedes ver los mensajes mas recientes donde te han mencionado, con el objeto de seguir y contestar conversaciones.

![](../static/img/chat/menciones.png)

* En el icono con bandera puedes ver los mensajes que haz marcado, estos pueden ser marcados haciendo usando la bandera que se encuentra junto a la marca de la hora en los mensajes.

![](../static/img/chat/mmarcados.png)

Si deseas mas informacion, consulta mas manuales en:

[docu.mazorca.org/mattermost](https://docu.mazorca.org/mattermost)
[Pagina de ayuda sobre mattermost](https://chat.mazorca.org/help/messaging)
