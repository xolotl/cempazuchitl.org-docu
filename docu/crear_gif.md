Title: Cómo crear un gif a partir de un video descargado de youtube
Slug: como-crear-un-gif-a-partir-de-un-video-descargado-de-youtube
Summary: Cómo crear un gif a partir de un video descargado de youtube
Date: 2017-07-14 12:00
Modified: 2017-07-14 20:40
Category: basicos
Tags: ffmpeg, youtube-dl, gif
Author: mazorca
Lang: es

Primero necesitamos un par de herramientas herramientas:

 <pre>
 apt install ffmpeg youtube-dl
 </pre>

y ahora descargamos un video para determinar el momento de inicio y fin del gif:

 <pre>
youtube-dl -f webm https://www.youtube.com/watch?v=lGP1YFE5s4M -o video.webm
 </pre>
 
ahora extraemos el segmento del video que queremos convertir a gif, reemplazando XX por el tiempo previo al inicio de fragmento, y YY por la duración del mimsmo.

<pre>
ffmpeg -ss XX:XX:XX -i video.webm -ss 00:00:00 -t YY:YY:YY -c copy fragmento.webm
</pre>

luego generamos un patrón para el gif, en el que reemplazamos YYY por el tamaño del lienzo (640)

<pre>
ffmpeg -y  -i fragmento.webm -vf fps=10,scale=YYY:-1:flags=lanczos,palettegen patron.png 
</pre>

después convertimos el video a gif

<pre>
ffmpeg   -i fragmento.webm -i patron.png -filter_complex  "fps=10,scale=640:-1:flags=lanczos[x];[x][1:v]paletteuse" archivo.gif
</pre>

y finalmente recortamos el gif para extraer la parte del lienzo que queremos, reemplazando XXX por el el ancho, YYY por el alto,  ZZZ por el margen izquierdo y AAA por el superior.

<pre>
convert archivo.gif -coalesce -repage 0x0 -crop XXXxYYY+ZZZ+AAA +repage final.gif
</pre>

los pasos exactos que seguí para generar este gif fueron

<pre>
~$ youtube-dl -f webm https://www.youtube.com/watch?v=lGP1YFE5s4M -o video.webm
</pre>

<pre>
~$ ffmpeg -ss 00:00:53 -i video.webm -ss 00:00:00 -t 00:00:05 -c copy fragmento.webm
</pre>

<pre>
~$ ffmpeg -y  -i fragmento.webm -vf fps=10,scale=640:-1:flags=lanczos,palettegen patron.png
</pre>

<pre>
~$ ffmpeg   -i fragmento.webm -i patron.png -filter_complex  "fps=10,scale=640:-1:flags=lanczos[x];[x][1:v]paletteuse" archivo.gif
</pre>

<pre>
~$ convert archivo.gif -coalesce -repage 0x0 -crop 320x300+150+000 +repage final.gif
</pre>
