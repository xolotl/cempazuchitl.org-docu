Title: Instalacion mattermost
Date: 2017-02-20 00:00
Category: mattermost
Tags: mattermost, chat, ssl, nginx, postgres, systemd, ssh, workinprogress

# IMPORTANTE: esta documentacion no debe ser usada para servidores en producción. Está en fase de revisión.

<pre>
aptitude install postgresql-contrib postgresql wget software-properties-common nginx openssl-y
su - postgres
psql
\password postgres
CREATE DATABASE mattermost;
CREATE USER mattermost WITH PASSWORD 'mattermost';
GRANT ALL PRIVILEGES ON DATABASE mattermost TO mattermost;
\q
exit
useradd -m -s /bin/bash matter
passwd mattermost
su - matter
wget https://releases.mattermost.com/3.6.1/mattermost-3.6.1-linux-amd64.tar.gz
cd mattermost
mkdir data
</pre>

config/config.json

<pre>
"SqlSettings": {
"DriverName": "postgres",
"DataSource": "postgres://mattermost:mattermost@tcp(localhost:3306)/mattermost?sslmode=disabled&connect_timeout=10&charset=utf8mb4,$
"DataSourceReplicas": [],
"MaxIdleConns": 20,
"MaxOpenConns": 300,
"Trace": false,
"AtRestEncryptKey": ""
},
</pre>

<pre>
cd bin
./plattform
</pre>

/etc/systemd/system/mattermost.service

<pre>
[Unit]
Description=Mattermost
After=syslog.target network.target
[Service]
Type=simple
User=mattermost
Group=mattermost
ExecStart=/home/mattermost/mattermost/bin/platform
PrivateTmp=yes
WorkingDirectory=/home/mattermost/mattermost
Restart=always
EstartSec=30
LomitNOFILE=49152
[Install]
WantedBy=multi-user.target
</pre>

<pre>
systemctl enable /etc/systemd/system/mattermost.service
systemctl status mattermost
systemctl restart mattermost
</pre>


/etc/nginx/sites-enabled/chat.mazorca.org


<pre>
server {
	listen 80;
	server_name chat.mazorca.org;
	#rewrite ^/(.*) https://chat.mazorca.org/$1;               # permanent;
	#root /var/www/html;
	#index index.html index.htm index.php;
	access_log   /var/log/nginx/chat.access.log combined;
	error_log   /var/log/nginx/chat.error.log warn;
}
	server {
        listen       443 ssl;
        server_name  chat.mazorca.org;
        access_log  /var/log/nginx/chat.access.log;
        error_log   /var/log/nginx/chat.error.log;
        ssl                  on;
        ssl_certificate      /etc/ssl/certs/ssl-cert-snakeoil.pem;
        ssl_certificate_key  /etc/ssl/private/ssl-cert-snakeoil.key;
        ssl_session_timeout  5m;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS";
	ssl_session_cache shared:SSL:10m;
	location / {
	gzip off;
		proxy_set_header X-Forwarder-Ssl on;
		client_max_body_size 50M;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;	
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header X-Frame-Options SAMEORIGIN;
		proxy_pass http://localhost:8065;
		proxy_pass_header Server;
		proxy_http_version 1.1;  # recommended with keepalive connections                                                    
       
	}
	access_log   /var/log/nginx/chat.-ssl-access.log combined;
	error_log   /var/log/nginx/chat-ssl-error.log warn;
}
</pre>
