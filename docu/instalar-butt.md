Title: Instalar butt en debian 
Slug: instalacion-butt-debian
Summary: Cómo instalar butt para transmitir audio a un servidor icecast
Date: 2017-06-16 12:00:00-06:00
Modified: 2017-06-16 12:00:00-06:00
status: draft 
Category: stream
Tags: stream, radio, butt, debian, jessie, make, configure
Author: mazorca
Lang: es

Lamentablemente este programa requiere de una librería que sólo está disponible en los repositorios no libres de debian, por lo que es necesario agregar "non-free" al final de la línea de los repositorios en /etc/apt/sources.list

 Y luego:

- obtener el código
 wget https://downloads.sourceforge.net/project/butt/butt/butt-0.1.15/butt-0.1.15.tar.gz?r=&ts=1497625715&use_mirror=svwh
- descomprimir
tar zxf butt-0.1.15/butt-0.1.15.tar.gz
- instalar dependencias
    sudo aptitude install libopus-dev libflac-dev libflac++-dev  libportaudio-ocaml-dev libsamplerate-dev libsamplerate0-dev libfdk-aac libaacs-dev  libvo-aacenc-dev libvoaacenc-ocaml-dev libfdk-aac-dev libfltk1.3-dev
- compilar
 cd butt-0.1.15/
- instalar
 ./configure && make && sudo make install



