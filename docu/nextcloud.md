Title: Cómo instalar y configurar nextcloud.
Date: 2017-03-26 15:37
Category: NextCloud
Slug: instalar-nextcloud
Summary: Cómo instalar y configurar nextcloud en tu servidor con debian jessie, usando LAMP y nginx como proxy reverso. (Documentación en proceso de elaboración. Úsela bajo su propio riesgo.).
Tags: NextCloud, nginx, apache, MariaDB
Author: xihh

**IMPORTANTE: Esta documentación se encuentra en desarrollo.
No debe ser usada en producción y por el momento no podemos garantizar que funcione.
Úsela bajo su propio riesgo :-p**

Nextcloud es una aplicación para tener tu propia nube,
compartir archivos, calendarios, etc.
Es software libre y tiene como prioridad la seguridad.

En este manual instalamos una instancia de NextCloud
detrás de un proxy reverso (nginx).

# Requisitos

Para instala nextcloud se necesita:

- Servidor web (Apache / Nginx)
- Base de datos (MariaDB)
- Intérprete PHP

Este conjunto es común para las aplicaciones web y se conoce como
LAMP (Linux Apache MariaDB PHP).

```
sudo apt-get update
sudo apt-get -y install \
        apache2 \
        git \
        libapache2-mod-php5 \
        mariadb-server \
        openssl \
        php5-curl \
        php5-fpm \
        php5-gd \
        php5-mysql
```

Al instalar MariaDB, el instalador pide la contraseña para administrar la base de datos.
Es preferible que se genere aleatoriamente y se guarde en un lugar seguro,
que puedan consultar todos los administradores cuando lo necesiten.

Instalar al mismo tiempo `apache2` y `nginx` genera un conflicto que no deja instalar el segundo.
Lo instalaremos al momento de configurar el proxy reverso.

# Preparar la base de datos

Para configurar la base de datos se ejecuta `mysql -u root -p`

```
# Generar la base de datos
CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
# Generar el usuario localhost
GRANT ALL PRIVILEGES ON nextcloud.* TO nextcloud@localhost IDENTIFIED BY 'my-contraseña-segura';
# Limpiar el almacén (en memoria) de los privilegios
FLUSH PRIVILEGES;
```

Hay que cambiar `'my-contraseña-segura'` por una contraseña segura.

# Descargar nextcloud

El enlace más actualizado para descargar la última versión en un comprimido zip
[se puede consultar aquí](https://nextcloud.com/install/#instructions-server ).

Nosotros descargamos el código del proyecto con el comando:

```
cd /var/www/
# hacer un directorio donde podamos escribir
sudo install -o `whoami` -g www-data -m 755 -d nextcloud
# descargar el código
git clone https://github.com/nextcloud/server.git nextcloud
# usar una versión estable
cd nextcloud
git checkout v11.0.2
# descargar el código de terceros
git submodule update --init
```

# Configurar el certificado seguro

Para conectarse de manera segura, se genera un certificado web.
La manera más sencilla es [generarlos con letsencrypt](https://docu.mazorca.org/generar-instalar-y-configurar-certificados-tlsssl-con-lets-encrypt-en-debian-jessie-y-nginx.html )
y [renovarlos automáticamente](https://nomia.mx/techno/https )

En debian jessie es necesario agregar el repositorio backports:

```
echo -e "deb https://ftp.debian.org debian jessie-backports main"\
| sudo tee /etc/apt/sources.list.d/jessie-backports.list
```

Y luego instalar certbot:

```
sudo apt-get update
sudo apt-get -y install \
        certbot
```

Para generar el certificado:

```
EMAIL="mazorca@riseup.net"
DOMINIO="xanadu.mazorca.org"
certbot  certonly --webroot -w /var/www/html/ -d "$DOMINIO" -m "$EMAIL"
```

# Configurar servidor web

Apuntamos el directorio de html a nuestro código de nextcloud:

```
sudo cd /var/www
sudo rm -r html/
sudo ln -s nextcloud html
```

Damos permisos al usuario de apache

```
chown www-data.www-data -R html/
```

Se editan los puertos que puede utilizar apache `/etc/apache2/ports.conf`:

```
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 8080

<IfModule ssl_module>
        Listen 443
</IfModule>

<IfModule mod_gnutls.c>
        Listen 443
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

Revisar la configuración y reiniciar el servicio:

```
apachectl -T \
&& systemctl restart apache2
```

# Configurar proxy reverso

En este momento ya le dijimos a apache que desocupe el puerto 80,
por lo que este comando ya funciona:

```
sudo apt install nginx
```

Se configura el nginx para para escuchar a apache `/etc/nginx/sites-available/xanadu.mazorca.org`:

<pre>
server {
        listen       443 ssl;
        server_name  xanadu.mazorca.org;

        access_log  /var/log/nginx/xanadu.access.log;
        error_log   /var/log/nginx/xanadu.error.log;

        ssl                  on;
        ssl_certificate      /etc/letsencrypt/live/xanadu.mazorca.org/fullchain.pem;
        ssl_certificate_key  /etc/letsencrypt/live/xanadu.mazorca.org/privkey.pem;

        ssl_session_timeout  5m;

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS";
        ssl_session_cache shared:SSL:10m;

        location / {
                gzip off;
                proxy_set_header X-Forwarder-Ssl on;
                client_max_body_size 50M;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
#                proxy_set_header Connection $connection_upgrade;

                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $remote_addr;
                proxy_set_header X-Forwarded-Proto $scheme;

                proxy_set_header X-Frame-Options SAMEORIGIN;
                proxy_pass http://localhost:8080;
                proxy_pass_header Server;
                proxy_http_version 1.1;  # recommended with keepalive connections
        }

        access_log   /var/log/nginx/xanadu.-ssl-access.log combined;
        error_log   /var/log/nginx/xanadu-ssl-error.log warn;

}
</pre>


Activar el sitio:

```
cd /etc/nginx/sites-enabled/
sudo rm default
sudo ln -s ../sites-available/xanadu.mazorca.org .
```

Comprobar que no haya errores y reiniciar el servicio:

```
sudo nginx -t \
&& sudo systemctl restart nginx
```

# Configurar nextcloud

Hay que dar permisos de escritura al usuario `www-data`
para que sea posible usar la interfaz web para configurar.

```
chown www-data:www-data -R html/
```

Entramos a la interfaz administrativa de nuestro servidor:

```
firefox "https://xanadu.mazorca.org"
```

Generar el usuario administrativo y configurar la base de datos:

![](/static/img/nextcloud/nc0d_1.png )
![](/static/img/nextcloud/nc0d_2.png )

## Activar el cifrado

Se puede activar el cifrado usando la interfaz web:

![](/static/img/nextcloud/cifrado0.png )
![](/static/img/nextcloud/cifrado1.png )

## Agregar aplicaciones

Entrando como administrador en la interfaz web:

![](/static/img/nextcloud/apps.png )

Buscar en las categorías, aquí se ejemplifica activar el calendario:

![](/static/img/nextcloud/calendar.png )

# PENDIENTES por documentar

- ¿Generar respaldos de la información?
- ¿Levantar varias instancias con la misma información?
- Actualizar el servicio.
