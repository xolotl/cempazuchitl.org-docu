Title: Recuperacion de Nextcloud (despues de un error en la actualizacion)
Slug: recuperacion-de-nextcloud
Summary: Despues de un error en la actualizacion de nextcloud 11 a Nextcloud es posible recuperar en unos sencillos pasos. 
Date: 2017-08-22 12:00
Category: NextCloud
Tags: ssh, basicos, servidores, web, nextcloud, recuperacion
Author: mazorca
Lang: es

Es importante aclarar que nuestra instalacion fue configurada de acuerdo a la documentacion que nos dejo [xihh](./author/xihh.html) en [Instalar y configurar nextcloud](./instalar-nextcloud.html). Asi que tambien esta en proceso de revision y no deberia usarse en un entorno de producción al pie de la letra.

#Copia de seguridad

Primero lo primero una copia de seguridad, creeme es mejor dejar todo como estaba, que no saber que se hizo.

```
# Copia de la aplicación
cp -R /var/www/nextcloud /var/www/nextcloud.bak
# Copia de la base de datos
mysqldump -u usuarioBDNextloud -p[usuario_password] [database_nextcloud] > dumpfilename.sql
# En caso de tener los datos de los usuarios en una ubicación diferente a /var/www/nextcloud/data (altamente recomendado) respaldar el directorio
cp -R /direccion/data /direccion/data.bak
```

Hay que hacer los cambios de `usuarioBDNextcloud`, `usuario_password`, `database_nextcloud`, `dumpfilename.sql`, `/direccion/data`; por los datos que tu usaste en tu instalacion.

#Descarga la ultima version

Usaremos git para mantener actualizada la aplicacion desde el codigo

```
#Creamos un directorio donde tengamos permisos de escritura 
sudo install -o `whoami` -g www-data -m 755 -d nextcloud12
# descargamos el codigo indicando un nombre diferente a la instalación
git clone https://github.com/nextcloud/server.git nextcloud12
# usar la version 12 marcada como estable
cd nextcloud12
git checkout stable12
# descargamos el código de terceros
git submodule update --init
```

#Configurar el acceso a la interfaz de nextcloud

```
#Removemos el enlace simbolico html
rm -r html
#Y creamos uno hacia Nextcloud12
sudo ln -s nextcloud12d html
#Otorgamos acceso al usuario www-data, para que el servidor web pueda hacer uso de los archivos contenidos
chown www-data:www-data -R nextcloud12/
#Accedemos a la interfaz de administracion web
firefox "https://xanadu.mazorca.org"
```

Configuramos un usuario administrador diferente al que ya existe (asignara el nuevo usuario al grupo de administradores y la base de datos con el mismo usuario y password que colocamos en la primera instalacion:

![](/static/img/nextcloud/nc0d_1.png )
![](/static/img/nextcloud/nc0d_2.png )


Si no sabes usuario y contraseña de la base de datos, puedes revisarla en: 
 ../nextcloud/config/config.php 
de tu actualizacion anterior

Si es necesario cambiamos la ruta donde se guardaran los archivos de los usuarios, es altamente recomendable ya que al estar en el directorio de publicacion web, podria originarse una fuga de informacion.

```
#Creamos otra carpeta donde podamos escribir
 sudo install -o `whoami` -g www-data -m 755 -d /datos/nexcloud/data
#Otorgamos acceso al usuario www-data, para que puedas tener acceso a los archivos
 chown www-data:www-data -R /datos/nextcloud/data
```

Al terminar puedes loggearte con la cuenta admin que creaste en la primera instalacion o con la que acabas de crear (ambos seran administradores

#Evitando el acceso a .git
Con esta configuración puedes actualizar con solo un `git pull`, pero el gran inconveniente es que es accesible desde internet el directorio `../nextcloud12/.git` por lo que deberas limitar el acceso:

```
sudo chmod -R 000 /nextcloud12/.git
```
