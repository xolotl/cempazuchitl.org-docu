Title: Cómo escribir documentación para Mazorca
Slug: como-escribir-docu-para-mazorca
Summary: Cómo escribir documentación para docu.mazorca.org
Date: 2017-02-20 12:00
Modified: 2017-06-01 20:40
Category: basicos
Tags: ssh, pelican, git, repo, documentación
Author: mazorca
Lang: es

Antes que nada vale decir que esta documentación se genera a partir de archivos redactados con markdown y compilados/exportados a html usando pelican.

Pero la magia detrás de su gestión es un servidor git implementado usando gitolite.

En este manual solo encontrarás cómo clonar/descargar el repositorio, y cómo (después de haber solicitado acceso de escritura), puedes corregir, aumentar y agregar nueva documentación.

Para ver cómo replicar el modelo, puedes visitar este otro LINK (no hay link). 

Dicho lo anterior, lo primero que se necesita es tener una llave ssh y pedir a alguna administradora que la agregue al repositorio git de la documentación.

## Clonar el respositorio {#clonar-repositorio}

Después de ello se puede "clonar" el directorio documentación con el siguiente commando:

<pre>
git clone ssh://git@mazorca.org:2281/docu
</pre>

Y después entrar al directorio recién clonado con:

<pre>
cd docu
</pre>

Al tratrse (la primera vez) de un repositorio completamente nuevo (recién clonado), no es necesario actualizarlo haciendo

<pre>
git pull
</pre>

Se debe tomar en cuenta que de ahora en adelante, para obtener los cambios hechos en el servidor, y antes de modificar cualquier archivo, es necesario ejecutar ese comando para obtener la copia más reciente y evitar problemas de inconsistencia en las versiones.

## Cómo hacer cambios en la documentación {#hacer-cambios}

Una vez se cuenta con un repositorio actualizado es posible hacer cambios a los archivos con el editor de texto de nuestra preferencia. Mientras escribamos con etiquetas markdown y html, nuestro texto se desplegará adecuadamente.

Después de haber hecho cambios en los archivos, se requieren 5 pequeños pasos más para que nuestras modificaciones se vean en la página web. 3 de estos debemos hacerlos cada unx de nosotrxs y 2 más deben ser hechos por las administradoras del servidor de documentación.

Los pasos necesarios de nuestro lado son:

agregar el o los archivos modificados a la lista de archivos para enviar con el comando

<pre>
git add ARCHIVO.md
</pre>

agregar una pequeña descripción de los cambios realizados

<pre>
git commit -m "pequeña descripción de los cambios realizados"
</pre>

empujar los cambios hacia el repositorio git de la documentación

<pre>
git push
</pre>

Finalmente, quienes administran el servidor deben obtener las modificaciones "empujadas" al repositorio con el comando 

<pre>
git pull
</pre>

(sí, del mismo modo como lo harían lxs demás) y compilar las modificaciones o archivos nuevos agregados ejecutando el comando 

<pre>
pelican
</pre>

en el servidor remoto.

## Cómo agregar documentación nueva {#agregar-documentacion}

Para Pelican (el motor usado en esta documentación), los archivos se dividen en 2 grandes partes, separadas por la primera línea en blanco del documento.

Cualquier texto arriba de la primera línea en blanco será interpretado como parte de los metadatos (fecha de publicación, categoría, idioma, autora, palabras clave, etc.). 

El texto que se encuentre debajo de la primera línea en blanco del documento, será propiamente el contenido del artículo.

En este artículo, por ejemplo, lo que antecede la primera línea en blanco del documento, luce así:

<pre>
Title: Cómo escribir documentación para Mazorca
Slug: como-escribir-docu-para-mazorca
Summary: Cómo escribir documentación para docu.mazorca.org
Date: 2017-02-20 12:00
Modified: 2017-03-22 12:00
Category: basicos
Tags: ssh, pelican, git, repo, documentación
Author: mazorca
Lang: es
</pre>

A la vez, el primer texto después de la primera línea en blanco es:

<pre>
Antes que nada vale decir que esta documentación se genera a partir de archivos redactados con markdown y compilados/exportados a html usando pelican.
</pre>

Es decir que las primeras 11 líneas de este documento, antes de ser compiladas por pelican para transformarlas de markdown a html, se ven exactamente asì:

<pre>
Title: Cómo escribir documentación para Mazorca
Slug: como-escribir-docu-para-mazorca
Summary: Cómo escribir documentación para docu.mazorca.org
Date: 2017-02-20 12:00
Modified: 2017-03-22 12:00
Category: basicos
Tags: ssh, pelican, git, repo, documentación
Author: mazorca
Lang: es

Antes que nada vale decir que esta documentación se genera a partir de archivos redactados con markdown y compilados/exportados a html usando pelican.
</pre>
## Subir un borrador (Que no se publique al subir)

A veces estamos trabajando en algun documento y no queremos que se publique aun, por que es necesario que los demas lo revisen, para esto en el mismo apartado de metadatos agrega la etiqueta:

<pre>
Status: draft
</pre>

## Cómo agregar un enlace {#agregar-enlace}

Igual que en html, en pelican hay al menos 3 tipos de enlaces/links. 

El primero consiste en enlazar a una url, el segundo en enlazar a una un ancla dentro de una url, y tercero a una ancla dentro del mismo documento.

En cualquiera de los casos, la sintaxis es muy parecida, y consiste usar braquets, corchetes y paréntesis para definir las diferentes partes del enlace.

Un ejemplo concreto para hacer un enlace a una página web sería:

<pre>
[Esto es un enlace a mazorca.org](https://mazorca.org)
</pre>

Además de eso, sin en mazorca.org hubiera una ancla con el id 'ancla1', podría vincularse directamente con algo como 

<pre>
[Enlace a mazorca.org](https://mazorca.org#ancla1)
</pre>

Si no se busca enlazar a una página interna sino a una dentro de este mismo servidor, se puede usar la etiqueta {filename}, de modo que 

<pre>
[Enlace]({filename}./agregar_documentacion.md)
</pre>

se comportaría exactamente del mismo modo que 

<pre>
[Enlace](https://docu.mazorca.org/como-agregar-documentacion.html)
</pre>

Finalmente, se puede enlazar a una ancla puesta dentro de un artículo en este servidor, con el siguiente código:

<pre>
[Enlace]({filename}./agregar_documentacion.md#agregar-enlace)
</pre>

el cual funcionaría exactamente igual que este otro:

<pre>
[Enlace](https://docu.mazorca.org/como-agregar-documentacion.html#agregar-enlace)
</pre>

