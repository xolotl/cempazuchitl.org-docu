Title: Cómo enviar un flujo de video a un servidor icecast usando liquidsoap
Slug: enviar-stream-video-icecast-liquidsoap
Summary: Como descargar videos de youtube, crear una lista de reproducción, y enviarlos como flujo contínuo a un servidor icecast usando liquidsoap.
Date: 2017-03-10 12:00
Modified: 2017-03-10 12:00
Category: Streaming
Tags: opam, liquidsoap, youtube-dl, icecast, stream, flujos
Author: mazorca
Lang: es

LiquidSoap es una herramienta basada en scripts que permite enviar flujos de audio y/o imágenes a servidores icecast, webcaster y shoutcast para que éstos a su vez permitan que el flujo dea escuchado o visualizado en reproductores o navegadorews web compatibles.

En este caso lo usaremos específicamente para enviar un flujo de video a un servidor icecast. En particular, enviaremos un flujo de videos provenientes de una lista de reproducción de videos descargados desde youtube.

## Instalación

Primero instalamos opam (que es el gestor de paquetes con el que instalaremos la versión más reciente de Liquidsoap) y algunas codecs para reproducir la mayor cantidad de formatos de video posibles...

<pre>
sudo apt install curl opam gstreamer1.0-plugins-bad gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly -y 
</pre>

Hecho lo anterior, inicializamos opam para con él instalar la versión más reciente de LiquidSoap

<pre>
opam init
</pre>

En seguida lo incorporamos al entorno del shell

<pre>
eval `opam config env`
</pre>

Ahora instalamos un paquete que ayuda a resolver dependencias

<pre>
opam install depext
</pre>

Y ahora revisamos e instalamos las dependencias necesarias.

<pre>
opam depext taglib mad lame vorbis cry liquidsoap gstreamer theora flac 
</pre>

Finalmente, instalamos LiquidSoap y algunos codecs y complementos para que procese y envíe los videos al servidor icecast.

<pre>
opam install taglib mad lame vorbis cry liquidsoap gstreamer theora flac 
</pre>


Ahora necesitamos algunos videos para repoducir, y aunque podemos usar videos producidos por nosotrxs mismos si los almacenamos en una carpeta, aprovecharemos esta docu para mostrart cómo  descargar una lista de videos usando youtube-dl

## youtube-dl


Primero descargamos e instalamos youtube-dl, que nos ayudará a descargar  algunos videos de youtube:

<pre>
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
</pre>

Después le damos permisos de ejecución:

<pre>
sudo chmod a+rx /usr/local/bin/youtube-dl
</pre>

Ahora creamos una carpeta para descargar los videos en ella:

<pre>
mkdir media
</pre>

Ingresamos a esa carpeta

<pre>
cd media
</pre>

Descargamos los videos de reproducción en formato webm (reemplazando la url por la que se quiera descargar)

<pre>
youtube-dl -f webm https://www.youtube.com/playlist?list=XXXXXXXXXXXXXXXXXXX
</pre>

Regresamos al directorio raíz

<pre>
cd ..
</pre>

## Lista de reproducción

Con el siguiente comando creamos una lista de reproducción que contenga todos los archivos descargados en la carpeta 'media'.

<pre>
find media -type f > videos.m3u
</pre>

## Script de emisión

Para finalizar, necesitamos un script que envíe el flujo  podemos usar el siguiente ejemplo, pero debemos cambiar algunos datos

 - ruta del binario de liquidsoap (que se puede obtener con el comando `which liquidsoap`); se pone en la primera linea reemplazando ' #!/home/k/.opam/system/bin/liquidsoap'
 - punto de montaje y contraseña del servidor icecast
 - calidad del la emisión (el script tiene 30 y debe ser un número entre 1 y 63)
 - tamaño del video (el script tiene 320 por 240)

<pre>

#!/home/k/.opam/system/bin/liquidsoap

set("decoder.file_decoders",["META","WAV","MIDI","FLAC","OGG","MAD","GSTREAMER"]) 
set("decoder.file_extensions.gstreamer",["mp4","webm"]) 
set("decoder.mime_types.gstreamer",["video/mp4","video/webm"])

set("log.stdout", true)
set("log.level",3)
set("log.file.path","flujo.log")

set("frame.video.width",320)
set("frame.video.height",240)
set("frame.video.samplerate",24)

playlist = mksafe(playlist("videos.m3u"))

source = fallback(track_sensitive=false, [playlist])

puntodemontaje="XXXX.webm"
password="XXXXXXXXXXXXXXX"
genero = "Videos insurrectos"
url = "https://mazorca.org"
host = "flujos.mazorca.org"
desc = "Descripción"
puerto= 8000

output.icecast(
        %ogg(%theora(quality=30),%vorbis),
        host=host,
        port=puerto,
        password=password,
        mount=puntodemontaje,
        genre=genero,
        description=desc,
        url=url ,
        source)

</pre>



Finalmente, debemos dar permisos de ejecución al script

	chmod +x nombre-del-script.liq

y 

ejecutarlo

	./nombre-del-script.liq
