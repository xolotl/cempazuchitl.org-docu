Title: Como instalar y configurar un servidor de icecast
Date: 2018-05-29 22:50
Category: radio
Tags: radio, acceso, airtime
Author: mazorca

#Como instalar y configurar un servidor de icecast
##¿Que es icecast?
Icecast es un servidor de streaming que permite a las emisoras de radio transmitir con eficacia en audio online (esto se conoce como la difusión por Internet). Es potente y estable, lo que significa que puede lanzar todo su tráfico de radio en Internet sin tener accidentes u otros problemas. Oyentes pueden acceder el audio streaming a través de cualquier reproductor de MP3 en vivo. Icecast tiene puntos de montaje (mountpoints) que de forma automática transfiere oyentes del Auto DJ a la transmisión en vivo y viceversa sin necesidad de activar manualmente cualquier cosa dentro o fuera del panel de control.
##Como se instala
Todos los comandos se deben ejecutar con permisos de administradr o super usuario.
Para instalar Icecast en nuestro servidor basta con abrir una terminal y agregar el siguiente comando:

**apt-get install icecast2 ices2 vorbis-tools -y**
Donde:
>**apt-get install** Nos permite instalar un paquete por su nombre desde los repositorios de nuestro sistema.

>**icecast2** Es el nombre para instalar el paquete del servidor de Icecast.

>**Ices2**  es un paquete se usa para proporcionar flujos de audio Ogg Vorbis a servidores de flujos Icecast 2. Permite usar tanto el audio en directo de una tarjeta de sonido como la recodificación de archivos Ogg Vorbis de una lista de reproducción.

>**vorvis-tools** es un paquete de herramientas que contiene contiene oggenc (un codificador), ogg123 (una herramienta de reproducción), ogginfo (muestra información de ogg), oggdec (decodifica archivos en ogg), vcut (divide archivos en ogg), y vorbiscomment (editor de comentarios de ogg).

>**-y** respondera automaticamente que SI (yes) cuando el sistema nos pregunte si estamos seguros de instalar los paquetes

Al pulsar la tecla “enter” automaticamente comenzara la instalaciòn y tendremos que espera hasta que nos aparezca la siguiente ventana:

![](../static/img/icecast/1.png)

En esta ventana tenemos dos opciones:

1. Configurar las contraseñas del servidor de icecast con el asistente de configuraciòn
2. No utilizar el asistente para posteriormente configurar manualmente las contraseñas.

En el caso de elegir utulizar la configuraciòn con el asistente al elegir la opcion <Si> nos aparecera la siguiente ventana:

![](../static/img/icecast/2.png)

Aqui deberemos colocar el nombre del servidor. Al pulsar aceptar nos enviara a la siguiente configuraciòn:

![](../static/img/icecast/3.png)

Aquí deberemos colocar la contraseña para los recursos de icecast. Al pulsar aceptar seguiremos con la siguiente configuraciòn:

![](../static/img/icecast/4.png)

Aqui tambien deberemos colocar una contraseña para el acceso a los repetidores de Icecast.
Seguimos con la siguiente ventana:

![](../static/img/icecast/5.png)

Aqui debemos colocar la contrseña de administrador con la cual finalizaremos el asistente de configuraciòn y daremos por terminada la instalaciòn de Icecast2 en nuestra terminal.
##Configuraciòn de Icecast
La configuracion de nuestro servidor de Icecast se guarda en un achivo con extension .xml que es un archivo de texto pero con la carcteristica de que puede ser interpretado por el programa en este caso Icecast.

Para abrir este archivo necesitaremos un editor de texto en modo terminal, para este caso utilizare “nano” que es un editor de texto que ya vienen instalado en Debian.

Entonces ejecutaremos el siguiente comando comando en la terminal:
**nano /etc/icecast2/icecast.xml**
donde:
>**nano** ejecuta el programa “nano” que es un editor de textos.

>**/etc/icecast2/icecast.xml** especifica la ruta del archivo que vamos a editar con nano, en este caso editaremos el archivo llamado icecast.xml en la ruta etc/icecast2/

Pulsamos la tecla “enter” y nos aparecera un documento de texto como este:

![](../static/img/icecast/6.png)


Aquí editaremos la configuraciòn de nuestro servidor. Las lineas que son antecedidas con “<!--” estan desactivadas y/o son comentarios explicativos de cada seccion.
Tratare de poner los parametros mas importantes y los que no se encuentren es preferible dejarlos como se encuentran o dirigirse a la documentaciòn oficial de Icecast http://icecast.org/docs/icecast-2.4.1/config-file.html:
>**< location >** 
Aquí debemos insertar la locación de nuestro servidor, no es necesario colocar la ubicaciòn real, basta con una palabra para identificar el servidor
**< admin >** 
Comunmente se coloca el contacto de quien administra el servidor para reportes y comunicaciòn en general.
**< limits >** 
Aqui se agrupan los parametros de limites dentro de los que se encuentran:
**< clients >** 
Numero de clientes o personas que pueden sintonizar la señal simultaneamente dentro del servidor.
**< sources >** 
Numero limite de personas que pueden mandar flujo simultaneamente al servidor.</sources>
**< queue-size >** 
Numeo de bytes de buffer, es decir, una fraccion de audio que se guarda para que cuando haya variables en la estabilidad del flujo no se pierda la conexión ni la continuidad de la transmision, en esta caso es donde se pierde la transmision en tiempo real con un retraso de algunos segundos.
**< authentication >**
Aqui se agrupan las contraseñas de administracion que vimos anteriormente se configuran con el asistente de configuracion durante la instlación, en el caso de haberlas agregado durante la instalacion aquí apareceran, en el caso de no haberlo hecho aquí es donde se pueden agregar y modificar las mismas.



##Crear puntos de montaje con usuario y contraseña

Para crear un punto de montaje con usuario y contraseña primero debemos tener en cuenta que quien tenga el usuario y contraseña de autenticacion podra conectarse al servidor pero es recomendable crear puntos de montaje especificos para cada usuario y asi tener un orden y registro de nustro servidor.

Para crear el punto de montaje con usuario y contraseña tenemos muchos parametros los cuales podemos modificar a nuestro gusto, en este caso utilizare los que considero son los mas importantes o con los que podemos hacer un punto de montaje con basico. en el caso de requerir modificar cada uno de los parametros considero visitar el manuel de usuario o la documentación ficial de Icecast.

En primer lugar pongo aqui una imagen de los parametros que tenemos disponibles y enlistare SOLO algunos con los que podremos empezar.

![](../static/img/icecast/7.png)

Ahora, para crear un punto de montaje de manera sencilla solo necesitamos definir algunos parametros:

>**< mount-name > ** 
Justo como dice el nombre aqui pondremos el punto de montaje comenzando con una / seguida del nombre del punto de montaje y la extension de archivo dependiendo el codec que usen para transmitir, puede ser ogg u mp3, de manera que si yo quisiera poner un punto de montaje llamado prueba 1 el parametro quedaria asi:
		 < mount-name > **/prueba1.mp3** < /mount-name > 
         
>**< username >**
Nombre de usuario que se colocara en el programa para hacer streaming, algunos progrmas no permiten modificar esta oción ya que por defecto usan el usuario >**source**, recomiendo usar algun programa que permita modificar este parametro. En el caso de usar un programa que si lo permita aqui se colocara este nombre de usuario.

>**< pasword>**
En este parametro tendremos que colocar la contraseña no del servidor sino la del usuario exclusivo del punto de montaje.

>**< max-listeners >**

>Aqui se colocara el parametro de escuchas limite para ese punto de montaje, esto varia dependiando de la audiencia y escuchas. Recuerda colocarlo con numero.

>**< dump-file >**
Dentro de este parametro podemos configurar una ruta en donde se hace el volcado de el streaming, es decir se va guardando todo lo que recive el servidor, en la comfiguracion predeterminada tenemos una ruta dentro de la carpeta **tmp** ya que si almacena todo sin borrar se puede llenar nuestro disco pero es una buena herramienta para tener respaldos que se lleguen a solicitar por los usuarios.

Basicamente con estos parametros podemos tener un punto de montaje con el cual se puede empezar a trabajar sin problemas.

para finalizar hay que revisar que nuestros parametros son correctos y/o los deseados para poder guardar el archivo con el mismo nombre **icecast.xml**, guardar y releer el archivo desde nuestra terminal con el comando:
**sudo systemctl reload icecast2.service**

Este comando nos permitirar releer el servicio y poner en marcha cada cambio que realizemos, por ejemplo agregar un nuevo punto de montaje.

Espero que esta documentación le sirva a alguien y pongan en marcha sus propios servidores para tomar los medios de comunicación y tener las herramientas necesarias para hacerlo.

Cualquier duda respecto al tutorial: xolotl(arroba)riseup(punto)net







