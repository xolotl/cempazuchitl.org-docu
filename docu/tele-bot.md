Title: Bot para Telegram
Slug: bot-telegram
Summary: Mini tutorial de como crear un bot para telegram
Date: 2017-08-11 22:06
Modified: 2017-08-11 22:06
Category: basicos
Tags: bot, telegram, documentación, python
Author: ZoRrO
Lang: es

# Creación de un Bot para telegram

El siguiente link es de una presentación muy sencilla que realice sobre la creación de los bot para telegram, espero sirva
como base para much@s.

[Presentación](https://presentacion.mazorca.org/tg-bot.html)
