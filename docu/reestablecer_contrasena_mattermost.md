Title: Reestablecer contraseña de usuarix y/o administradira mattermost
Date: 2017-02-20 00:00
Category: mattermost
Tags: mattermost, postgres, cli

Mattermost ofrece una interfaz de linea de comandos para hacer muchas kosas a nivel adiministrativo, incluyendo cambiar la contraseña de administradora o de cualquier otrx usuarix.

el comando para hacerlo es tan sencillo como 

	$RUTABASE/bin/platform user password nombre_de_usuarix CONSTRASEÑA_NUEVA -c $RUTABASE/config/config.json

Pero como ese comando da por hecho que sabemos el nombre_de_usuarix, y es posible que no tengamos ese dato, estos son los pasos para obtenerlo:

inicio de sesión como usuario admin de la base de datos:

	~$ su - postgres

inicio de shell de la base de datos:

	~$ psql

Conexión con la base de datos:

	\c base_de_datos

mostrar listado de las tablas en la base de datos:

	\dt

mostrar descripción (columnas y tipo de datos) en la TABLA 'users':

	\d TABLA 

mostrar usuarios con privilegios de administración:

	select username from users where roles LIKE '%system_admin%' ;

Ahora que tenemos el nombre_de_usuarixs con privilegios de administración, podemos cambiarlo usando el comando del incio del tutorial, pero antes debemos cerrar las sesiones que iniciamos para llegar hasta ese punto.

salir del shell del servidor de bases de datos:

	\q

salir de la sesión de usuario de base de datos:

	~$ exit

ejecutar el comando para cambiar la contraseña:


	~$ $RUTABASE/bin/platform user password USUARIx CONSTRASEÑA_NUEVA -c $RUTABASE/config/config.json



