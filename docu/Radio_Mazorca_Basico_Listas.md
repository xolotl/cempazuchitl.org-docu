Title: Radio Mazorca Básico Listas
Date: 2017-02-20 10:20
Category: radio
Tags: radio, acceso, airtime
Author: mazorca

Ingresar a la consola web en la url <http://airtime1d.mazorca.org/> o vía tor <http://g2a3glebu7ugn4uo.onion/>

![](../static/img/airtimeBasico/Airtime_184.png)     ![](../static/img/airtimeBasico/Airtime_186.png)

### Crear listas de reproducción

#### Fija

* Basándonos en la búsqueda simple, presionamos el botón «Open Media Builder»

![](../static/img/airtimeBasico/Airtime_202.png)


* Presionamos sobre «Nuevo» y nos muestra un menú en el cual seleccionamos «Nueva lista de reproducción»

![](../static/img/airtimeBasico/Airtime_204.png)


* En la nueva ventana seleccionamos del lado izquierdo las canciones que nos interesan dando click en el recuadro 

![](../static/img/airtimeBasico/Airtime_205.png)


* Ya sea que demos clic sobre «Añadir a la lista de reproducción actual» o los arrastremos al área de lista de reproducción

![](../static/img/airtimeBasico/Airtime_206.png)


* Presionar sobre «Lista de reproducción sin nombre» para asignar uno.
* Es recomendable presionar sobre «Ver / editar descripción» para definir un detalle sobre los temas.

![](../static/img/airtimeBasico/Airtime_207.png)

* «Guardar»


#### Dinámica

* Basándonos en la búsqueda, presionamos el botón «Open Media Builder»

![](../static/img/airtimeBasico/Airtime_202.png)


* Presionamos sobre «Nuevo» y nos muestra un menú al cual seleccionamos «Nuevo bloque inteligente»

![](../static/img/airtimeBasico/Airtime_204.png)


* El bloque inteligente tiene dos opciones «Configura un tipo de bloque inteligente:»
	* Estático,  crea una lista buscando con un patrón que definamos y la genera en este momento.
	* Dinámico,  crea una lista pero es generada buscando el patrón al momento de reproducirse.



* Para el ejemplo se usara «Dinámico» 
* Presionamos en «Bloque inteligente sin nombre» para asignar uno
* La regla de buqueda 

![](../static/img/airtimeBasico/Airtime_209.png)  ![](../static/img/airtimeBasico/Airtime_211.png)

Indicamos el tiempo de duración

* Si permitimos repetir pistas en caso de no tener suficientes para la duración

![](../static/img/airtimeBasico/Airtime_208.png)

* Presionando en las + se agregan campos de busqueda
![](../static/img/airtimeBasico/Airtime_208a.png)

* «Guardar»


## Temas
* [Básico](radio-mazorca-basico.html)
* [Subir audio](radio-mazorca-basico-subir.html)
* [Programación](radio-mazorca-basico-programar.html)
