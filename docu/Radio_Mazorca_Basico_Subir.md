Title: Radio Mazorca Básico Subir
Date: 2017-02-20 10:20
Category: radio
Tags: radio, acceso, airtime
Author: mazorca

Ingresar a la consola web en la url <http://airtime1d.mazorca.org/> o vía tor <http://g2a3glebu7ugn4uo.onion/>

![](../static/img/airtimeBasico/Airtime_184.png)     ![](../static/img/airtimeBasico/Airtime_186.png)

## Subir audios
* Al presionar sobre el botón «AÑADIR PISTAS» podemos ver una nueva ventana

![](../static/img/airtimeBasico/Airtime_190.png)


* Presionar sobre el botón «Agregue archivos»
![](../static/img/airtimeBasico/Airtime_191.png)


* Seleccionamos los archivos a subir
![](../static/img/airtimeBasico/Airtime_192.png)


* Presionar el botón «Comenzar Subida»
![](../static/img/airtimeBasico/Airtime_194.png)


* Mientras suben los primeros archivos se pueden seleccionar mas y dejarlos en espera.



## Biblioteca de audio
Una vez que se subió el audio o para ver los audios ya existentes 

* Presionar el botón «BIBLIOTECA». Se puede observar los últimos subidos
![](../static/img/airtimeBasico/Airtime_196.png)

### Editar un archivo

* Ponemos el puntero del ratón sobre el registro del archivo y presionar el botón izquierdo del ratón
![](../static/img/airtimeBasico/Airtime_198.png)


* De las opciones que muestra el menú emergente, si somos el dueño del audio. 
	* *Previsualizar*, permite escuchar el audio
	* *Eliminar*, borra el archivo del sistema
	* *Editar metadata*, ajustar los valores del archivo
	* *Descargar*, obtener una copia del archivo
* La opción «Editar metadata» nos muestra una nueva ventana donde podemos cambiar los registros informativos de la grabación.

![](../static/img/airtimeBasico/Airtime_199.png)

* Estos valores serán mostrados al momento de enviar el audio si la aplicación de recepción los despliega. 
* «Guardar»
* Si no eres el dueño del archivo las únicas opciones que muestra es:
	* *Previsualizar*, permite escuchar el audio
	* *Descargar*, obtener una copia del archivo



### Buscar temas
En la mis ventana de «BIBLIOTECA» podemos localizar audios en dos formas

#### Simple

* En donde se muestra una barra con el icono de lupa, basta poner una cadena

![](../static/img/airtimeBasico/Airtime_200.png)

#### Avanzada

* Si presionamos sobre la leyenda «Opciones de búsqueda avanzada» muestra una nueva sección de la pagina en donde tenemos otras opciones de consulta.

![](../static/img/airtimeBasico/Airtime_201.png)


* Presionando nuevamente en la leyenda «Opciones de búsqueda avanzada» se ocultan las búsquedas avanzadas


## Temas
* [Básico](radio-mazorca-basico.html)
* [Listas de reproducción](radio-mazorca-basico-listas.html)
* [Programación](radio-mazorca-basico-programar.html)
