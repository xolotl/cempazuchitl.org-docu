Title: Radio Mazorca Básico Programar
Date: 2017-02-20 10:20
Category: radio
Tags: radio, acceso, airtime
Author: mazorca

Ingresar a la consola web en la url <http://airtime1d.mazorca.org/> o vía tor <http://g2a3glebu7ugn4uo.onion/>

![](../static/img/airtimeBasico/Airtime_184.png)     ![](../static/img/airtimeBasico/Airtime_186.png)

## Programar un espacio
* Después que los encargados asignan un espacio se podrá agregar audio solamente a ese tiempo asignado, para ver los programas presionar en «CALENDARIO»

![](../static/img/airtimeBasico/Airtime_212.png)


* Como se puede ver hay icono rojo indica que no tiene audio programado. Los iconos amarillos indican que hay audio pero no suficientes para cubrir el horario. Sin icono hay audio suficiente para cubrir el horario. 
* Para este ejemplo usaremos el programa del sábado 18 de las 11hrs.  Presionamos con el botón izquierdo del ratón y nos da un menú
	* Agregar/eliminar contenido, asignar audio al espacio.
	* Eliminar todo el contenido, borrar todo el audio programado al espacio.
	* Mostar el contenido, indicar el audio que se asigno al horario.

![](../static/img/airtimeBasico/Airtime_213.png)

* Esto solo se muestra si tenemos permisos de editar el espacio. Si ya pasó o no tenemos permiso solo veremos «Mostrar el contenido»
* Al presionar sobre «Agregar/eliminar contenido» nos muestra una nueva ventana muy parecida a la de búsquedas y creación de listas.

![](../static/img/airtimeBasico/Airtime_214.png)


* La idea es la misma, buscar un patrón y agregarla al horario o incluso buscar las listas creadas para cubrir el espacio. La ventaja de usar listas es que se pueden repetir al solo seleccionar un registro y no buscar elemento por elemento. Como en este caso buscamos la lista Totlasohcuic, seleccionamos y presionamos sobre «Añadir al show seleccionado» 

![](../static/img/airtimeBasico/Airtime_215.png)


* Se puede apreciar que la lista ahora incluye una columna con la hora de inicio, hora final en base a la duración de la canción y de los tiempos del programa. 
* También se puede apreciar al final un numero negativo en rojo que indica el tiempo faltante para cubrir el tiempo del programa. 
* Se puede ordenar seleccionando un elemento y moviéndolo arriba o abajo. Seleccionar nuevas canciones o eliminar algún elemento del actual. 
* «OK»


## Temas
* [Básico](radio-mazorca-basico.html)
* [Subir audio](radio-mazorca-basico-subir.html)
* [Listas de reproducción](radio-mazorca-basico-listas.html)
