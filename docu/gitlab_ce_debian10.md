# Instalar Gitlab CE en debian 10 Buster

Nota importante: Este es un primer acercamiento a la instalacion de Gitlab CE para pruebas y de ninguna manera debe ser utilizado para servidores en producción ya que es mas complejo y depende de muchas otras cosas como Postfix, certificados de seguridad, SMTP, firewall, etc.
Asi que es para introducirse o "jugar" con el servicio.

Nota: Todos los comandos deben ser ejecutados como super usuario o root
  para eso debes utilizar el comando su - (en debian 10 ahora se utiliza "su -" y no "su")

##Actualizar el sistema
  apt update && apt upgrade -y

## Instalar los paquetes necesarios para la Gitlab CE
  apt -y install curl openssh-server ca-certificates

## Instalar el servidor de correo Postfix (en este documento no profundizare en la configuracion de Postfix ya que escribire esto mas adelante y por el momento nos conformaremos con la configuracion por default)
  apt install postfix -y
  Nota: elegir "no configurar postfix" en el menu que aparece al instalarlo

##Agregar el repositorio de Gitlab CE en debian 10
 curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash

## Actualizar despues de agregar el repositorio
  apt update

## Instalar Gitlab CE
  gitlab-ce

## Reconfigurar Gitlab CE para su primer uso
 gitlab-ctl reconfigure

## Definir usuario root
Al ingresar por primera vez a la direccion IP de nuestro servidor que aloja Gitlab CE debemos elegir una contraseña para el administrador
Imagen1

## Ingresar al servicio Gitlab por primera vez
Al ingrear la contraseña de root ya podremos logearnos en el servicio como administradores
Imagen2

##Bienvenido a Gitlab
Ahora ya puedes crear proyectos y comenzar a explorarlo localmente
Imagen3

happy hacking by xolotl
