Title: Como instalar un servidor de Libretime
Date: 2018-07-08 16:20
Category: radio
Tags: radio, acceso, libretime, streaming, software libre, linux
Author: mazorca

#Como instalar un servidor de libretime
##¿Que es Libretime?
Libretime es un administrador de radio basado en Airtime desarrollado por Sourcefabric para la administracion de radio online, el cual ya no siguio con el desarollo del mismo pero el programa es funcional hasta la fecha (Julio 2018). Libretime es un Fork de Airtime el cual esta optimizado y actualizado.

Libretime es un sofware libre que nos permite administrar una estacion de radio de forma remota y de manera local por medio del navegador web, nos permite mantener una biblioteca online para albergar musica, podcast, etc. Tambien podemos editar los metadatos de las pistas para hacer listas de reproduccion inteligentes basadas en las caracteristicas deseadas por el usuario, podemos a su vez crear enlaces, grabar programas y retrasnmitirlos, tener un calendario con horarios, ademas de tener una interface facil e intuitiva.

##Como instalar Libretime

Todos los comandos se deben ejecutar con permisos de administradr o super usuario, en este caso utilizaremos el sistema operativo Ubuntu en su version 16.04. Para instalar Libretime en nuestro servidor basta con abrir una terminal y agregar los siguientes comandos:

**1.-   apt-get install git**
Donde:
>**apt-get install** Nos permite instalar un paquete por su nombre desde los repositorios de nuestro sistema.
>**git** instala el paquete llamado git que nos permitira clonar el programa desde su repositorio.

**2.- git clone https://github.com/LibreTime/libretime.git**
Donde:
>**git clone** Nos permitira clonar en nuestro disco duro la carpeta del pryecto desde su repositorio.
>**https://github.com/LibreTime/libretime.git** es el link a la carpeta contenedora.

**cd libretime**
>Nos permitira acceder a la carpeta descargada

**./install**
>Ejecutara un script que automatizara la instlacion de airtime con una imagen como esta:
![](../static/img/libretime/1.png)

Ahora nos pedira una serie de indicaciones de "si o no" con las que deberemos responder y a las tres como en la imagen:
![](../static/img/libretimli/2.png)
![](../static/img/libretime/3.png)
![](../static/img/libretime/4.png)
Al finalizar el script ya tendremos instalado el servidor de libretime en la direccion web local http://localhost

##Ultimos pasos:
Para terminanar con la instalacion nos dirigiremos a http://localhost o a la direccion del servidor donde instalamos libretime, en donde encontraremos lo siguiente:
![](../static/img/libretime/5.png)
donde podremos configurar la base de datos, en este caso dejaremos todos los valores por defecto y solo presionaremos el boton "next".
En el siguiente paso tambien dejaremos los valores por defecto y daremos click en next:
![](../static/img/libretime/6.png)
Lo mismo haremos con la pagina siguiente, todo lo dejaremos como esta o configuraremos segun las necesidades de cada usuario:
![](../static/img/libretime/7.png)
E igual con la siguiente ventana:
![](../static/img/libretime/8.png)

Para finalizar tendremos que agregar los comandos que nos aparecen en pantalla con los cualaes iniciaremos los servicios de libretime:
![](../static/img/libretime/9.png)
Los comandos son los de la imagen, aun asi los pondre enlistados por orden:
1- **sudo service airtime-playout start**
2- **sudo service airtime-liquidsoap start**
3- **sudo service airtime_analyzer start**
4- **sudo service airtime-celery start**

ahora podremos dar click en "Done!" para ir a la siguiente pagina en donde se nos muestra el status de libretime:
![](../static/img/libretime/10.png)
Si todo aparece bien significa que nustro libretime esta listo para usarse, ahora damos click en el boton de "login" y nos dirigira a la pagina principal de libretime:
![](../static/img/libretime/11.png)

al dar click en "login" podremos acceder al panel de control de libretime con usuario, contraseña y seleccionando el idioma.

![](../static/img/libretime/12.png)
Recuerda que el usuario por defecto es :**admin**
Y la contraseña por defecto es **admin**

ahora ya tenemos nuestro servidor de libre time listo para trabajar.
![](../static/img/libretime/13.png)

Espero que esta documentación le sirva a alguien y pongan en marcha sus propios servidores para tomar los medios de comunicación y tener las herramientas necesarias para hacerlo.

Cualquier duda respecto al tutorial: xolotl(arroba)riseup(punto)net o mazorca(arroba)riseup(punto)net


 
