Title: Creación de dominio .onion (TOR)
Date: 2017-02-20 10:20
Category: TOR
Tags: tor, web, ssh, acceso, onion
Author: mazorca

## cómo crear un dominio .onion en  4 sencillos pasos

Una vez que hemos entendido qué es TOR y tenemos muy claro que sólo una pequeña parte de quienes lo usan son traficantes de drogas prohibidas, de de armas o pedòfilos, y sabiendo que muy pocxs de quienes tienen el mundo verdaderamente jodido (empresarios, congresistas, alcaldes y policías) están ahí, podríamos estar interesadxs en crear un dominio onion para acceder y dar acceso a un servicio de manera anìnima y cifrada.

El procedimiento consta de 4 pasos:

- instalar los paquete necesarios
- determinar el/los puertos que estarán disponibles en un archivo de configuraciòn.
- reiniciar el demonio tor para crear los dominios y,
- obtener y distribuir (si se desea) el dominio .onion generado.

Como en este tutorial asumimos que el servicio que queremos exponer a la red TOR es una pàgina web, damos por hecho que ya hay un servidor web corriendo en el servidor. Si faltara ese paso, se puede hacer siguiendo este otro tutorial. LINK

En caso de querer montar otro servicio (servidor ssh, mail, git, irc, etc), lo que verdaderamente necesitamos es el puerto que usan.

Y dicho lo anterior, instalamos el ùnico paquete indispensable.

	~# apt-get install torsocks

Eso crearà una carpeta y un archivo de configuraciòn por defecto que podemos encontrar en:

	/etc/tor/torrc

Como la mayorìa de los archivos de configuración, es un documento en texto plano, asì que podemos editarlo con 

	~# nano /etc/tor/torrc

En él encontraremos (entre muchas otras) estas lìneas:

	#HiddenServiceDir /var/lib/tor/hidden_service/
	#HiddenServicePort 80 127.0.0.1:80

Lo que debemos hacer con ellas es descomentarlas y editarlas  (o agregar unas nuevas), para establecer el nombre y puerto del servicio.

En el caso de un servidor web configurado en el puerto standard (80), quedarìan asì:

	HiddenServiceDir /var/lib/tor/web/
	HiddenServicePort 80 127.0.0.1:80

En el caso se un servidor ssh usando también el puerto standard(22), podrìan quedar asì:

	HiddenServiceDir /var/lib/tor/ssh/
	HiddenServicePort 22 127.0.0.1:22

Una vez salvados los cambios y de vuelta en el shell del sistema, debemos reiniciar el demonio tor para que al leer nuevamente el archivo de configuración, cree la llave de encriptación privada, y escriba en un archivo el nombre del dominio .onion.

	~# systemctl restart tor

Como la ruta que establecimos en el archivo de configuraciòn fue "/var/lib/tor/web/", en ella encontraremos tanto la llave privada como el nombre de dominio.

Es recomendable hacer una copia de seguridad de la llave privada  porque si ella se pierde el dominio .onion se pierde con ella.
Dicha llave está almacenada en: 

	/var/lib/tor/web/private_key

Finalmente, podemos acceder al nombre del dominio .onion creado, con el siguiente comando.


	~# cat /var/lib/tor/web/hostname

En el caso específico del servidor en el que se hospeda en esta página, la salida es:

	vm37lyofffctr45l.onion

así que alguien navegando por tor podrìa usar esta url para ver esta misma pàgina:

	http://vm37lyofffctr45l.onion/creacion-de-dominio-onion-tor.html

Happy hacking!
