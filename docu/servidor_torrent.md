Title: Servidor de Torrents Remoto
Slug: crear-gestionar-servidor-de-torrent
Summary: Cómo crear y gestionar un servidor de torrents via remota
Date: 2017-03-22 12:00
Modified: 2018-01-18 17:01
Category: Torrent
Tags: ssh, torrent, magnet, documentación, cli
Author: ZoRrO/Pixelead0
Lang: es

# Contenido.
#### 1.- Instalación  de un servidor de Torrent.
#### 2.- Creación de Torrents y enlaces magnet.
#### 3.- Script de automatizacion.
#### 4.- Introducción a la interfaz web de transmission.

# 1.-INSTALACIÓN DE UN SERVIDOR TORRENT
>**Nota: si estas en un entorno con interfaz gráfica lo mas recomendable es utilizar el programa `transmission` de la siguiente forma.**

En derivados de Debian, para instalar lo necesario, el comando es:

```sh
$ sudo apt install transmission-cli transmission-common transmission-daemon
```

Con esto se instalara el demonio y la interfaz web a utilizar.

Para configurar la interfaz web hay que modificar el archivo:

```sh
/etc/transmission-daemon/settings.json
```

Y modificar los siguientes valores quedando así el archivo:

```sh
rpc-authentication-required: true,
rpc-enabled: true,
rpc-password: {080807e9618e58b6e310400759126e0b3d5af0149MLOoAWM",
rpc-port: 9090,
rpc-url: /transmission/,
rpc-username: "zorro",
rpc-whitelist: "127.0.0.1",
```
**Explicación:**

| Opción | Descripción |
| ------ | ------ |
|rpc-enabled | `false/true`_ activar o desactivar la interfaz web |
|rpc-authentication-required | `false/true`_ activar o desactivar la opción de usuario y contraseña para la interfaz web(se recomienda usar `true`)|
|rpc-password | Antes de cambiar esta opción es recomendable parar el servicio del demonio con `/etc/init.d/transmission-daemon stop` poner entre las comillas la contraseña/password a usar, guardar los cambios e iniciar nuevamente el servicio con `/etc/init.d/transmission-daemon start`, automáticamente en el archivo de configuración la contraseña se convertirá en un hash para no dejar la contraseña en un texto plano, por eso en el ejemplo aparece `{080807e9618e58b6e310400759126e0b3d5af0149MLOoAWM`.|
|rpc-port | puerto a usar para la interfaz web, puedes elegir el que desees.|
|rpc-url | URL principal|
|rpc-username | nombre del usuario|
|rpc-whitelist | aquí se recomienda poner la ip o rango de ip's a usar para conectarse, en el ejemplo se usa la de la computadora local(127.0.0.1) pero si tu servidor se encuentra en tu mis red, deberás usar el rango que tengas asignado o introducir la ip desde donde te conectaras.|

Dentro del archivo _`settings.json`_, hay muchas mas opciones que se pueden modificar como es la ruta de descarga por default, las sesiones torrent entre otras cosas que por tiempo no explicaremos en detalle en este momento.

# 2.- CREACIÓN DE TORRENTS Y ENLACES MAGNET
Si usas transmission con interfaz gráfica la combinación de "ctrl + N" te abrirá el menú para crear un archivo torren directamente.
[imagen](https://compartir.mazorca.org/#htVTVkLeuLd9rq6SSz8IZw)

## Crear archivo Torrent:

### Sintaxis:
```sh
$ transmision-create -o ARCHVO.torren -t TRAKERS /ruta/de/carpeta/o/archivo
```

#### Ejemplo:
```sh
$ transmission-create -o Los.Vigilantes.torrent -t udp://tracker.openbittorrent.com:80 -t udp://tracker.opentrackr.org:1337 -t udp://tracker.coppersurfer.tk:6969 -t udp://tracker.leechers-paradise.org:6969 -t udp://zer0day.ch:1337 -t udp://explodie.org:6969 /var/www/Pelikulas/Los.vigilantes/
```

#### Crear enlace Magnet:

En la interfaz grafica al agregar o ya en un archivo listo y abrir el menu contextual(click derecho) aparecerá la opcion de copiar el enlace magnet.

##### Sintaxis:
```sh
$ transmission-show -m /ruta/de/archivo/torrent
```
##### Ejemplo:
```sh
$ transmission-show -m Torrents/Los.Vigilantes.torrent
```

####Agregar enlace a _**transmission**_ desde consola

Ademas se puede agregar un archivo o enlace magnet creado u obtenido de algún otro lado a tu demonio de _**transmission**_ 
directamente desde la consola, evitando así el uso de la interfaz web en aquellos casos donde la interfaz
web de problemas.

#####Sintaxis:
```sh
$ transmission-remote ip.del.servidor:puerto.del.servidor -a archivo.torrent -n usuario:contraseña
```
####Ejemplo:
```sh
$ transmission-remote 127.0.0.1:9098 -a Los.Vigilantes.torrent -n zorro:Mazorca.2017

o

$ transmission-remote 127.0.0.1:9098 -a magnet:?xt=urn:btih:01184814c1efd45b993a668463a58eda53889016&dn=Los.vigilantes&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969 -n zorro:Mazorca.2017
```

_Donde la opción "-u" es para señalar al usuario y contraseña establecidos para la interfaz web si así fue activada._

# 3.- Script de automatización.

A continuación se explicara el manejo de un script escrito con el proposito de: 

1. Automatizar la creación de los torrent.
2. Guardar los archivos creados en una carpeta especial designada.
3. Crear un archivo especial de texto donde se almacenara el enlace magnet del archivo creado.

```sh
#!/bin/bash
####Datos Modificables
u=zorro					#Usuario
pass=Mazorca.2017			#Contraseña
ser=127.0.0.1				#Ip de tu servidor, si es local puedes dejar el que esta
puer=9098				#Puerto que le asignaste, por default es el que esta.
carp=/home/zorro/Torrents/		#Carpeta donde se guardaran los torrent creados
arch=/home/zorro/enlaces.magnet.txt	#Archivo donde se irán guardando los enlaces magnet creados

transmission-create -o $carp$1.torrent -t udp://tracker.openbittorrent.com:80 -t udp://tracker.opentrackr.org:1337 -t udp://tracker.coppersurfer.tk:6969 -t udp://tracker.leechers-paradise.org:6969 -t udp://zer0day.ch:1337 -t udp://explodie.org:6969 $2
echo " "
transmission-remote $ser:$puer -a $carp$1.torrent -n $u:$pass
echo " "
echo $1 >> $arch
transmission-show -m $carp$1.torrent >> $arch
tail -n 2 $arch
```

El archivo se ejecuta dando dos parámetros de inicio, el primero es el nombre del archivo torrent que se creara(sin la extensión .torrent) y el
segundo es la dirección de la carpeta o archivo del cual se creara el torrent.

####Instalación

Copia y pega el contenido anterior en un archivo de texto y renombrarlo como desees(para este ejemplo utilizaremos 
el nombre de "creador.T.sh") con la extensión _.sh_. En su defecto si lo deseas descarga el siguiente archivo _[Script](static/torrentcreacion.T.sh)_.

Ya sea que copiaste el texto o descargaste el archivo deberás cambiar los parámetros que se te indican en el mismo archivo, utiliza el
editor de texto de tu preferencia:

| Opción | Descripción |
| ------ | ------ |
| u | El nombre del usuario que se definió en el archivo settings.json |
| pas | La contraseña/password del usuario |
| ser | La ip del servidor donde se ejecuta el demonio de transmission, si es tu maquina local la ip 127.0.0.1 debería funcionar |
| puer | El puerto que usa el demonio de transmission, por default es el 9098 |
| carp | La dirección de la carpeta donde se desea que se guarden los archivos torrent al ser creados |
| arch | Ubicación/nombre del archivo donde se desee que se guarde los enlaces magnet de los torrent creados |

Una vez modificado el archivo hay que otorgarle permisos de ejecución en las propiedades del mismo o con el comando _chmod +x creador.T.sh_.
Seguido se sugiere mover el archivo a la ubicación en **/usr/bin/**, que es la carpeta de los archivos instalados propios del usuario, y de
este modo poder llamar al comando/script desde cualquier consola.

#### Resumen de comandos.

```sh
nano creador.T.sh
_Modificar los parámetros desde el editor_
chmod +x creador.T.sh
sudo mv creador.T.sh /usr/bin
```

####USo del Script.(sintaxis)

```sh
$ creador.T.sh nombre.del.torrent /ubicación/del/archivo/
```

####Ejemplo

```sh
$ creador.T.sh Los.Vigilantes /home/zorro/Pelikulaz/Los.Vigilantes
```


# 4.-INTERFAZ GRÁFICA WEB DE TRANSMISSION
[Menu Principal](https://compartir.mazorca.org/#b_VUlzRO34eAvCVySLcKrg)

[Menu para subir](https://compartir.mazorca.org/#1Uro3PlzQRh7U3Y02wE9zg)

La opción _**examinar**_ te permite agregar cualquier archivo torrent que se encuentre en tu computadora local.

_**Or enter URL**_ permite agregar directamente cualquier enlace magnet

Y por ultimo se elige el directorio donde se desea que se realice la descarga, si en el directorio elegido se encuentra ya el archivo completo, transmssion comprobara automáticamente la integridad de este y si es en un 100%  correcta se convertirá en una semilla para compartir.

Dejar la opción _**Start when add**_ activada, para realizar tanto la descarga y compartición de forma automática al iniciar.

Por ultimo Usar la interfaz web o el `transmission-gtk` para abrir el enlace y seleccionar la carpeta local
