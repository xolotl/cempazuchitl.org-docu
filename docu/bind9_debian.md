Title: Servidor de nombres de dominio (dns) con bind
Date: 2017-03-20 10:20
Category: basicos
Tags: DNS, bind, RED
Author: mazorca
Summary: La implementacion de un servidor DNS en una red puede reemplazar las direcciones IP de las maquinas por su nombre

# Bind9

**Contenidos**
  
1. Presentacion
2. Definiciones
3. Network Layout
4. Administracion del servidor
    1. Instalacion
    2. Configuracion
        1. TSIG firma
        2. Archivo: /etc/bind/named.conf
        3. Archivo: /etc/bind/named.conf.default-zones
        4. Archivo: /etc/bind/named.conf.options
        5. Archivo: /etc/bind/named.conf.local
        6. Archivo: /etc/bind/named.conf.log
    3. Registro de Recursos (RR)
        1. Archivos en var/cache/bind
        2. Algunas explicaciones
    4. Archivo: /etc/resolv.conf
5. Bind Chroot
6. Manejo de clientes
7. Herramientas de prueba
8. Enlaces y recursos

##**Presentacion**

La implementacion de un servidor DNS en una red puede reemplazar las direcciones IP de las maquinas por su nombre. Como resultado, es posible incluso asociar varios nombres a una misma maquina para actualizar los diferentes servicios disponibles. Por ejemplo, www.ejemplo.com y pop.ejemplo.com podrian apuntar al servidor primario donde se encuentran el servidor de correo y la intranet empresarial y el dominio podria ser ejemplo.com. Es facil recordar que estos dos servicios se ejecutan en la misma maquina cuya direccion IP es 192.168.0.1.

Ahora imagina que el administrador de red decide por una u otra razon mover el servidor de correo a la maquina 192.168.0.11. Lo unico que debe cambiarse es el archivo de configuracion del servidor DNS. Aunt tienes la oportunidade de ir y editar el archivo hosts a todos los usuarios, pero esto tardaria demasiado tiempo y podria molestar a algunas personas.

##**Definiciones**

* **DNS**: Sistema de nombres de dominio o servidor de nombres de dominio
* **Servidor Primario**: Tambien denominados _**maestro**_. Obtiene los datos del dominio a partir de un archivo alojado en el mismo servidor
* **Servidor Secundario**: Tambien denominado _**esclavo**_. Al iniciar obtiene los datos del dominio a traveś de un Servidor Maestro (o primario), realizando un proceso denominado **transferencia de zona**.
* **Cache del servidor**:

##**Network Layout**

Tenemos acceso a internet a traves de un xxxbox(192.168.1.1), dos servidores DNS proporcionados por nuestro ISP (80.10.249.2, 80.10.249.129). De hecho, estos dos ultimos servidores no seran mencionados en la configuracion porque el xxxbox se encargara de resolver los nombres si no se conoce el destino del paquete. En consecuencia se considera el xxxbox como un servidor primario fuera de nuestro dominio. El servidor *"sid" (192.168.1.10)* esta conectado al xxxbox a traves de su interfaz primaria de red. Tambien esta conectado a la LAN (192.168.0.0/24) por su interfaz secundaria de red (192.168.0.1). Es en este donde vamos a instalar el servidor DNS primario para nuestro dominio *ejemplo.com* [RFC2606](https://tools.ietf.org/html/rfc2609). A todos los equipos de la LAN se les asigna automaticamente una direccion unica por el servidor DHCP. El DHCP tambien proporciona la direccion del servidor DNS principal para nuestro dominio y actualiza los nombres de host de la zona *ejemplo.com* para que puedan asociarse con una direccion IP.

##**Administracion del servidor**

###**Instalacion**

Actualizamos nuestro sistema:

        #apt-get install & apt-get safe-upgrade

Instalamos el paquete bind9:

        #apt-get install bind9

Tambien puedes instalar la documentacion (muy util):

        #apt-get install bind9-doc

###**Configuracion**

Despues de la instalacion, es posible que desee familiarizarse con algunos de los archivos de configuracion. Estan en el directorio /etc/bind/

####**Firma TSIG**

El proposito de esta firma es autentificar transacciones con BIND. Por lo tanto el servidor DHCP no puede actualizar el dominio ejemplo.com si pierde esta clave. Copiar y pegar una clave existente.

        # cd /etc/bind/
        # cat rndc.key
        key "rndc-key" {
                algorithm hmac-md5;
                secret "QJc08cnP1xkoF4a/eSZZbw==";
        };
        
        # cp rndc.key ns-example-com_rndc-key

Puede generar una nueva clave con las siguientes opciones:

* **algorithm HMAC-MD5** - identificador 157 (necesario para la firma TSIG y solo al algoritmo suportado por BIND

* **length of 512 octets** (multiplo de 64 con un maximo de 512 para el algoritmo anterior)

* **name**: nombre para la clave 

        dnssec-keygen -a HMAC-MD5 -b 512 -n USER ns-example-com_rndc-key
        Kns-example-com_rndc-key.+157+53334

La huella asociada con la clave es 53334. Se crean dos archivos uno con extension **key** y el otro con extensión **private**. Esta sustituye la clave del archivo ns-example-com_rndc-key por la presente en uno de estos archivos.

        # cat Kns-example-com_rndc-key.+157+53334.private
        Private-key-format: v1.2
        Algorithm: 157 (HMAC_MD5)
        Key: LZ5m+L/HAmtc9rs9OU2RGstsg+Ud0TMXOT+C4rK7+YNUo3vNxKx/197o2Z80t6gA34AEaAf3F+hEodV4K+SWvA==
        Bits: AAA=

        # cat ns-example-com_rndc-key
        key "ns-example-com_rndc-key" {
                algorithm hmac-md5;
                secret "LZ5m+L/HAmtc9rs9OU2RGstsg+Ud0TMXOT+C4rK7+YNUo3vNxKx/197o2Z80t6gA34AEaAf3F+hEodV4K+SWvA==";
        };
        
Por razones de seguridad el archivo ns-example-com_rndc-key no debe ser accesible por el mundo. Este debe insertarse en la configuracion de bind por una inclusion ya que la configuracion de bind en si misma es legible por todo el mundo. Ademas, es una buena idea borrar los archivos **key** y **private** generados previamente.

####**Archivo /etc/bind/named.conf**

Este es el archivo con la configuracion principal del DNS

        // Managing acls
        acl internals { 127.0.0.0/8; 192.168.0.0/24; };
        
        // Load options
        include "/etc/bind/named.conf.options";
        
        // TSIG key used for the dynamic update
        include "/etc/bind/ns-example-com_rndc-key";
        
        // Configure the communication channel for Administrative BIND9 with rndc
        // By default, they key is in the rndc.key file and is used by rndc and bind9 
        // on the localhost
        controls {
                inet 127.0.0.1 port 953 allow { 127.0.0.1; };
        };
        
        // prime the server with knowledge of the root servers
        zone "." {
                type hint;
                file "/etc/bind/db.root";
        };
        
        include "/etc/bind/named.conf.default-zones";
        include "/etc/bind/named.conf.local";

Nota: con Debian Jessie la *'zone "." {...}'* esta dentro del archivo *"named.conf.default-zones"*. No necesitas agregarla en el archivo *"named.conf"*.

####**Archivo /etc/bind/named.conf.default-zones**

Nota a partir de Debian 7 "Wheezy" bind9 se empqueta con un archivo que contiene las zonas forward, reverse y broadcast.

        // be authoritative for the localhost forward and reverse zones, and for
        // broadcast zones as per RFC 1912
        zone "localhost" {
                type master;
                file "/etc/bind/db.local";
        };
        zone "127.in-addr.arpa" {
                type master;
                file "/etc/bind/db.127";
        };
        zone "0.in-addr.arpa" {
                type master;
                file "/etc/bind/db.0";
        };
        zone "255.in-addr.arpa" {
                type master;
                file "/etc/bind/db.255";
        };

####**Archivo /etc/bind/named.conf.options**

Este archivo contiene todas las opciones de configuracion para el servidor DNS

        options {
                directory "/var/cache/bind";
        
                // Exchange port between DNS servers
                query-source address * port *;
        
                // Transmit requests to 192.168.1.1 if
                // this server doesn't know how to resolve them
                forward only;
                forwarders { 192.168.1.1; };
        
                auth-nxdomain no;    # conform to RFC1035
        
                // Listen on local interfaces only(IPV4)
                listen-on-v6 { none; };
                listen-on { 127.0.0.1; 192.168.0.1; };
        
                // Do not transfer the zone information to the secondary DNS
                allow-transfer { none; };
        
                // Accept requests for internal network only
                allow-query { internals; };
        
                // Allow recursive queries to the local hosts
                allow-recursion { internals; };
        
                // Do not make public version of BIND
                version none;
        };

El puerto asociado a la opcion **query-source** en ningun caso debe estar congelado por que pone en peligro las transacciones DNS en el caso de una resolucion

 * [Nota de vulnewrabilidad VU#800113](http://www.kb.cert.org/vuls/id/800113)
 * [Envenenamiento de cache DNS en Bind9](http://www.trusteer.com/bind9dns)

M. Rash escribio un interesante articulo acerca de esto y como forzar al puerto de origen aleatorio a traves de iptables: [Mitigar los ataques de envenenamiento de cache con iptables](http://www.cipherdyne.org/blog/2008/07/mitigating-dns-cache-poisoning-attacks-with-iptables.html)

Para reducir el tiempo de espera para las conexiones UDP y asi destacar la asignacion al azar, que por defecto es de 30 segundos por tupla, simplemente actualice el parametro net.filter.nf_conntrack_udp_timeout

        #sysctl -w net.netfilter.nf_conntrack_udp_timeout=10

para obtener un timeout de 10 segundos.

####**Archivo /etc/bind/named.conf.local**

Este archivo contiene la configuracion local del servidor DNS, y aqui se declaran las zonas asociadas con los dominios del servidor.

        // Manage the file logs
        include "/etc/bind/named.conf.log";
        
        // Domain Management example.com
        // ------------------------------
        //  - The server is defined as the master on the domain.
        //  - There are no forwarders for this domain.
        //  - Entries in the domain can be added dynamically 
        //    with the key ns-example-com_rndc-key
        zone "example.com" {
                type master;
                file "/var/lib/bind/db.example.com";
                //forwarders {};
                // If we do not comment the ''forwarders'' "empty" clients of the local subnet in my case don't have access to the upstream DNS ?
                //allow-update { key ns-example-com_rndc-key; };
                allow-update { key rndc-key; };
                //confusion between the file name to import (ns-example-com_rndc-key) and the key label (rndc-key) ?
        };
        zone "0.168.192.in-addr.arpa" {
                type master;
                file "/var/lib/bind/db.example.com.inv";
                //see comment below (zone "example.com")
                //forwarders {};
                //allow-update { key ns-example-com_rndc-key; };
                allow-update { key rndc-key; };
        };
        
        // Consider adding the 1918 zones here, if they are not used in your
        // organization
        include "/etc/bind/zones.rfc1918";

Nota: si haz creado un non-FQDN local y es llamado .local choca con  otros paquetes (¿cuales?). Edite /etc/nsswitch.conf y mueva los permisos dns despues de los archivos, en la linea *host*, hasta hacer que el dominio local trabaje.

####**Archivo /etc/bind/named.conf.log**

Con debian Jessie necesitas crear este archivo en /etc/bind/

        logging {
                channel update_debug {
                        file "/var/log/update_debug.log" versions 3 size 100k;
                        severity debug;
                        print-severity  yes;
                        print-time      yes;
                };
                channel security_info {
                        file "/var/log/security_info.log" versions 1 size 100k;
                        severity info;
                        print-severity  yes;
                        print-time      yes;
                };
                channel bind_log {
                        file "/var/log/bind.log" versions 3 size 1m;
                        severity info;
                        print-category  yes;
                        print-severity  yes;
                        print-time      yes;
                };
        
                category default { bind_log; };
                category lame-servers { null; };
                category update { update_debug; };
                category update-security { update_debug; };
                category security { security_info; };
        };

Aqui definimos diferentes metodos de registro para las diferentes categorias. La primera categoria es, como su nombre lo indica la categoria por default y usualmente es asignada a syslog. Todas las categorias no mencionadas, son similares a la categoria por default. Para una lista de  las diferentes categorias, consulta [manual de referencia del administrador bind9](http://www.bind9.net/manual/bind/9.3.2/Bv9ARM). En terminos de servidores blade, ignora todos los registros asociados con ellos.

##**Resource Records (RR)**

DNS se compone de varios registros, RR o registros de recursos, que definen la informacion de varios dominios. El primero esta dedicado a la resolucion de nombres, en este ejemplo, es el archivo db.example.com. El segundo sera usado para la resolucion de nombres inversa, este es el archivo db.example.com.inv

####**Archivos en /var/cache/bind**

* RR para la resolucion de nombres (archivo db.example.com)

        $TTL    3600
        @       IN      SOA     sid.example.com. root.example.com. (
                           2007010401           ; Serial
                                 3600           ; Refresh [1h]
                                  600           ; Retry   [10m]
                                86400           ; Expire  [1d]
                                  600 )         ; Negative Cache TTL [1h]
        ;
        @       IN      NS      sid.example.com.
        @       IN      MX      10 sid.example.com.
        
        sid     IN      A       192.168.0.1
        etch    IN      A       192.168.0.2
        
        pop     IN      CNAME   sid
        www     IN      CNAME   sid
        mail    IN      CNAME   sid

* RR para la resolucion de nombres inversa

        @ IN SOA        sid.example.com. root.example.com. (
                           2007010401           ; Serial
                                 3600           ; Refresh [1h]
                                  600           ; Retry   [10m]
                                86400           ; Expire  [1d]
                                  600 )         ; Negative Cache TTL [1h]
        ;
        @       IN      NS      sid.example.com.
        
        1       IN      PTR     sid.example.com.
        2       IN      PTR     etch.example.com.

####**Algunas explicaciones**

**$TTL**: (Time To live, tiempo de vida) expresa la duracion en segundos en que es valido por defecto la informacion contenida en los RR. Una vez expirado este tiempo es necesario volver a comprobar los datos. Tipos:

    * **SOA**: Start Of Authority, especifica la informacion autoritoria sobre un zona DNS.

Para definir informacion sobre el area, en este caso el nombre del servidor DNS primario "sid.example.com". y la direccion de correo electronico del contacto tecnico (root.example.com, el @es reemplazado por un punto). Se compone de varios campos:

        * *Serial*: es el conjunto no firmado de 32 bits, Este es el numero de serie a incrementar con cada cambio de archivo. Permite al servidor secundario recargar la informacion que tiene. El proposito general es darle formato YYYYMMDDXX, ya sea para la primera correccion 01/04/2007 -> 2007040101, para la segunda 2007040102.

        * *Refresh* Defiene el tiempo que esperara para actualizar los datos

        * *Retry* Si se produce un error al actualizar, define el tiempo que espera antes de reintentar actualizar.

        * *Expire* Despues de este tiempo, sin recibir respuesta,  el servidor se considera como no disponible

        * *Negative Cache TTL* tiempo de vida que tendra una respuesta NXDOMAIN

        * *NS* Define una lista de servidores de nombres con autoridad para un dominio.

        * *MX* Define una lista de servidores de correo para un dominio, es posible darles una prioridad asignando un numero, entre menor sea el numero, mayor sera la prioridad.

        * *A* Asocia el nombre de un host a una direccion IPv4

        * *AAAA* Asocia el nombre de un host a una direccion IPv6

        * *CNAME* Identifica el nombre canonico de un alias (un nombre que apunta a otro nombre)

        * *PTR* Asocia una direccion IPv4 a un nombre de host (inverso a lo que hace un registro A)

Las clases en la asociacion determinan la clase de internet. Otras clases estan disponibles (CH y HS). Para mas informacion por favor consulte el [RFC 1035]{Agregar direccion}.

###**Archivo /etc/resolv.conf**

        search example.com

No es mas complicado que esto

##**Bind chroot**

El nombre del demonio usado para iniciar bind es el usuario por default.

Esta opcion se encuentra en el archivo de configuracion del servicio bind **/etc/default/bind9** (NOTA: esto no es valido para jessie que usa systemd):

        OPTIONS="-u bind"

El script que inicia bind **/etc/init.d/bind9** lee este archivo de configuracion cuando el servicio es iniciado.

Una buena practica es no iniciar bind como root, pero para correr el demonio en un entorno chroot, necesitamos especificar el directorio chroot. Esto se hace utilizando la misma variable _OPTIONS_ en **/etc/default/bind9**.

Para empezar, detenga el servicio bind:

        /etc/init.d/bind9 stop

A continuacion edite **/etc/default/bind9** (no para jessie)

        OPTIONS="-u bind -t /var/bind9/chroot"

Para Jessie, cree el archivo **/etc/systemd/system/bind9.service**  con las opciones "_-t /var/bind9/chroot_"

        [Unit]
        Description=BIND Domain Name Server
        Documentation=man:named(8)
        After=network.target
        
        [Service]
        ExecStart=/usr/sbin/named -f -u bind -t /var/bind9/chroot
        ExecReload=/usr/sbin/rndc reload
        ExecStop=/usr/sbin/rndc stop
        
        [Install]
        WantedBy=multi-user.targe

Para Jessie, despues de crear el archivo anterior, actualice el symlink del archivo con:

        systemctl reenable bin9

Ahora cree la estructura de directorio chroot:

        mkdir -p /var/bind9/chroot/{etc,dev,var/cache/bind,var/run/named}

Cree los archivos especiales de dispositivo requeridos y asigne los permisos correctos

        mknod /var/bind9/chroot/dev/null c 1 3
        mknod /var/bind9/chroot/dev/random c 1 8
        chmod 660 /var/bind9/chroot/dev/{null,random}

Mueva el directorio de configuracion actual al nuevo directorio chroot:

        mv /etc/bind /var/bind9/chroot/etc

Ahora cree el enlace simbolico en /etc para la compatibilidad

        ln -s /var/bind9/chroot/etc/bind /etc/bind 

Si desea usar la zona horaria local en el chroot (por ejemplo para syslog)

        cp /etc/localtime /var/bind9/chroot/etc/

Cambie el propietario de los archivos que acaba de mover y el resto de la estructura de directorios chroot creados recientemente:

        chown bind:bind /var/bind9/chroot/etc/bind/rndc.key
        chmod 775 /var/bind9/chroot/var/{cache/bind,run/named}
        chgrp bind /var/bind9/chroot/var/{cache/bind,run/named}

Edite la variable _PIDFILE_ en **/etc/init.d/bind9 a la ruta correcta:

        PIDFILE=/var/bind9/chroot/var/run/named/named.pid

Finalmente pida a rsyslog que envie los logs de bind al lugar correcto:

        echo "\$AddUnixListenSocket /var/bind9/chroot/dev/log" > /etc/rsyslog.d/bind-chroot.conf

Reinicie rsyslog e inicie bind

        /etc/init.d/rsyslog restart; /etc/init.d/bind9 start

##**Manejo de clientes**

Como lo mencione al comenzar, la asignacion de direcciones IP en una LAN es realizado por un servidor DHCP. Asi, para establecer nuestro servidor DNS a los diferentes clientes, es necesario agregar al archivo de configuracion DHCP dos lineas:

        option domain-name "example.com
        option domain-name-server sid.example.com

Deberia agregarse al archivo (yo creo) las areas para las cuales DHCP debe realizar automaticamente las actualizaciones.

Sintaxis (todo despues de "_=>_" son mis comentarios):

        zone [name.of.the.zone.]{
                primary 127.0.0.1; => El servidor DNS se encuentra ee la misma maquina que el DHCP
        key rndc-key; => Es necesario proporcionar la clave de seguridad (por un include) en el comiezno del archivo de configuracion del servidor DHCP. Esta debe ser la misma clave de seguridad que permite la actualizacion para la zona en el named.conf.local de bind9
        }

Ejemplos de [name.of.the.zone.] (con el "**.**" en el final)

-example.com.: para la zona directa de este articulo,
-0.168.192.in-addr.arpa.: para la zona inversa de este articulo

Para mas informacion en la implementacion de la actualizacion dinamica de registros DNS a traves de DHCP, consulte el manual de DHCP (NT:, el link en la version original esta indisponible)

##**Herramientas de prueba**

**Comando dig**: Puede buscar en el servidor DNS que elija y obtener una gran cantidad de informacion, ademas de la resolucion de nombres y contrastar la resolucion.

        $ dig nomade-frjo.stones.lan
        ; <<>> DiG 9.4.2 <<>> nomade-frjo.stones.lan
        ;; global options:  printcmd
        ;; Got answer:
        ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15760
        ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 2
        
        ;; QUESTION SECTION:
        ;nomade-frjo.stones.lan.                IN      A
        
        ;; ANSWER SECTION:
        nomade-frjo.stones.lan. 900     IN      A       192.168.0.242
        
        ;; AUTHORITY SECTION:
        stones.lan.             604800  IN      NS      emerald.stones.lan.
        stones.lan.             604800  IN      NS      diamond.stones.lan.
        
        ;; ADDITIONAL SECTION:
        diamond.stones.lan.     604800  IN      A       192.168.0.1
        emerald.stones.lan.     604800  IN      A       192.168.0.2
        
        ;; Query time: 20 msec
        ;; SERVER: 127.0.0.1#53(127.0.0.1)
        ;; WHEN: Fri Mar 28 20:53:09 2008
        ;; MSG SIZE  rcvd: 131
        
        $ dig -x 192.168.0.242
        ; <<>> DiG 9.4.2 <<>> -x 192.168.0.242
        ;; global options:  printcmd
        ;; Got answer:
        ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37702
        ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 2
        
        ;; QUESTION SECTION:
        ;242.0.168.192.in-addr.arpa.    IN      PTR
        
        ;; ANSWER SECTION:
        242.0.168.192.in-addr.arpa. 900 IN      PTR     nomade-frjo.stones.lan.
        
        ;; AUTHORITY SECTION:
        0.168.192.in-addr.arpa. 604800  IN      NS      diamond.stones.lan.
        0.168.192.in-addr.arpa. 604800  IN      NS      emerald.stones.lan.
        
        ;; ADDITIONAL SECTION:
        diamond.stones.lan.     604800  IN      A       192.168.0.1
        emerald.stones.lan.     604800  IN      A       192.168.0.2
        
        ;; Query time: 19 msec
        ;; SERVER: 127.0.0.1#53(127.0.0.1)
        ;; WHEN: Fri Mar 28 20:53:31 2008
        ;; MSG SIZE  rcvd: 155
                

**Comando NSLOOKUP**: Algo lento, pero aun es util

        $ nslookup etch
        Server:         192.168.0.1
        Address:        192.168.0.1#53
        Name:   etch.example.com
        Address: 192.168.0.2
        
        $ nslookup 192.168.0.2
        Server:         192.168.0.1
        Address:        192.168.0.1#53
        2.0.168.192.in-addr.arpa        name = etch.example.com.

**Comando named-checkconf**: Verifica la sintaxis de configuracion en los archivos de Bind9

        # named-checkconf -z
        zone localhost/IN: loaded serial 1
        zone 127.in-addr.arpa/IN: loaded serial 1
        zone 0.in-addr.arpa/IN: loaded serial 1
        zone 255.in-addr.arpa/IN: loaded serial 1
        zone estar.lan/IN: loaded serial 20080315
        zone 0.168.192.in-addr.arpa/IN: loaded serial 20080315
        zone 10.in-addr.arpa/IN: loaded serial 1
        zone 16.172.in-addr.arpa/IN: loaded serial 1
        zone 17.172.in-addr.arpa/IN: loaded serial 1
        zone 18.172.in-addr.arpa/IN: loaded serial 1
        zone 19.172.in-addr.arpa/IN: loaded serial 1
        zone 20.172.in-addr.arpa/IN: loaded serial 1
        zone 21.172.in-addr.arpa/IN: loaded serial 1
        zone 22.172.in-addr.arpa/IN: loaded serial 1
        zone 23.172.in-addr.arpa/IN: loaded serial 1
        zone 24.172.in-addr.arpa/IN: loaded serial 1
        zone 25.172.in-addr.arpa/IN: loaded serial 1
        zone 26.172.in-addr.arpa/IN: loaded serial 1
        zone 27.172.in-addr.arpa/IN: loaded serial 1
        zone 28.172.in-addr.arpa/IN: loaded serial 1
        zone 29.172.in-addr.arpa/IN: loaded serial 1
        zone 30.172.in-addr.arpa/IN: loaded serial 1
        zone 31.172.in-addr.arpa/IN: loaded serial 1
        zone 168.192.in-addr.arpa/IN: loaded serial 1

**Comando named-checkzone**: Verifica la validez de los archivos de zona antes de resetear la configuracion.

        # named-checkzone example.com /var/lib/bind/db.example.com
        zone example.com/IN: loaded serial 20080315
        OK
        
        # named-checkzone 0.168.192.in-addr.arpa /var/lib/bind/db.example.com.inv
        zone 0.168.192.in-addr.arpa/IN: loaded serial 20080315
        OK
 

##Enlaces y recursos

[RFC 1034](https://www.rfc-es.org/rfc/rfc1034-es.txt)
[RFC 1035 Especificaciones e implementacion](https://www.ietf.org/rfc/rfc1035.txt)
[RFC 1592 Estructura y delegacion de sistema de nombres de dominio](https://www.rfc-es.org/rfc/rfc1591-es.txt)
[RFC 2609 Nombres DNS reservados de primer nivel](https://www.rfc-es.org/rfc/rfc2606-es.txt)
[Manual del administrador de bind9]()
