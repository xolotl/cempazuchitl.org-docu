Title: Repos salvajes de gitolite
Slug: repos-salvajes
Summary: Cómo permitir que los usuarios de gitolite puedan generar repos fácilmente
Date: 2017-05-20 21:18:27-05:00
Modified: 2017-03-22 12:00
Category: basicos
Tags: gitolite, git, repo, gestión
Author: mazorca
Lang: es

Cómo hacer repositorios nuevos en nuestro servidor de git.

[gitolite](http://gitolite.com/gitolite/index.html "Revisa que quien se conecta tenga permisos para usar el repo.")
se encarga de revisar quién(es) puede(n) conectarse a qué repositorio(s) de git
alojado en un [servidor].

Los permisos se manejan usando un (otro) repositorio de git llamado `gitolite-admin`.
Allí hay dos carpetas:

```
/ruta/a/gitolite-admin
├── conf
│   └── gitolite.conf # aquí se configuran los permisos
└── keydir
    ├── mapache.pub   # estas son claves públicas de los usuarios
    ├── pato@home.pub # un usuario puede tener varias llaves públicas
    ├── pato@work.pub
    ├── peto.pub
    ├── usuario1.pub
    └── zorro.pub

2 directories, 6 files
```

Gitolite tiene una función que permite a los usuarios generar repos en una carpeta
con el simple hecho de hacer push a un repositorio que no existe.

La función se llama [repositorio salvaje](http://gitolite.com/gitolite/wild/#wild-repos-user-created-repos "El manual en inglés.")
y para usarlo se agrega a `/ruta/a/gitolite-admin/conf/gitolite.conf`:

```
# alias para anotar a todos los usuarios
# se podría usar @all, pero entonces cualquiera podría escribir aún sin tener llave.
@users = mapache pato peto usuario1 zorro 

# se utiliza un lenguaje para describir patrones de texto
# llamado «expresiones regulares» para describir "todo lo que está en el directorio mazorca"
# 
repo mazorca/.*
# ¿quiénes pueden crear repo?
# la C (C de crear) debe ir sola y en su propio renglón:
  C = @users
# quiénes pueden leer (R de read), escribir (W de write) y generar o borrar ramas (+)
  RW+ = @users
```

Ahora para crear un repositorio, se puede ejecutar el comando:

```
git push mi.servidor.de.git.org:mazorca/repo.git
```

Si ya tienes trabajo hecho en un repo de git que sólo tienes en tu computadora,
puedes subirlo siguiendo estas instrucciones:

```
# decirle a git dónde va a guardar el repositorio,
# hay que cambiar todo lo que está entre corchetes:
git remote add origin [usuario]@[mi.servidor.de.git]:mazorca/[repo].git

# decirle a git que la rama llamada «master» debe siempre subirla a «origin»,
# el repositorio remoto que indicamos en el comando pasado
git push -u origin master
```
Finalmente, si alguien dentro del grupo de usuarixs (@users) quiere clonar el repo, puede hacerlo con 

```
git clone git.mazorca:mazorca/REPO
```
