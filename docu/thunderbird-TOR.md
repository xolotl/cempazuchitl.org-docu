Title: Thunderbird bajo Tor
Slug: thunderbird-bajo-tor
Summary: Como configurar nuestro Thunderbird para usarlo con la red Tor
Date: 2018-01-17 12:32
Modified: 2018-01-17 12:32
Category: TOR
Tags: Thunderbird,tor,documentación
Author: ZoRrO
Lang: es

# Contenido.
#### 1.- Introducción.
#### 2.- Configuración Básica.
#### 3.- Configuración Avanzada.

## 1.-Introducción

Hola, con este muy sencillo tutorial lograremos configurar nuestro gestor de correos favoritos ( ;) ) [Thunderbird](https://www.thunderbird.net/es-MX/) y la red
de [Tor](https://www.torproject.org/).

Como recordatorio, diremos que la red Tor es la encarga de proveernos de anonimato en la red/internet (que no es lo mismo que seguridad, pero se llevan de la mano), para
este tutorial debemos tener instalo previamente tor en nuestras computadoras ( si usas [Debian](https://www.debian.org) o alguno de sus derivados, es tan fácil de 
instalar como con un *$ apt install tor* y ya con eso tendremos corriendo tor en nuestras compus con los parámetros por default, que entre los importantes es que
ahora eres parte de la red tor y por ende con tu aportación se vuelve mas grande y rápida).

Thunderbird es el gestor de correos de muchas distribuciones Linux por default, y seguramente se puede instalar en cualquier otra con mucha facilidad, su uso radica
principalmente en la posibilidad de administrar múltiples cuentas de correo en un solo gestor, con el beneficio de contar con miles de plugin que pueden hacer tu
vida mucho mas fácil y cómoda.

Para finalizar la introducción solo deseo comentar y recalcar que el uso de Tor y Thunderbird nos servirá desde darnos anonimato y la posibilidad de lograr saltar 
algunos bloqueos de instituciones y/o gobiernos, pero de nada servirá si tenemos malos hábitos en seguridad, contraseñas débiles o proveedores de servicios de poca/nula
confianza,etc.

Una buena practica seria tener un proveedor de correos confiable (en este caso se usara los servicios de [Riseup](https://riseup.net/), que pos
son de confi), tener un contraseña segura y lo suficiente robusta (recuerda que no se necesita ser exageradamente complicada para que sea segura o puedes usar
algún gestor de contraseñas), activar el cifrado de tu servidor (si es que esta disponible, Riseup, si lo permite) y usar cifrado con GPG en tus correos (que igual 
Thunderbird tiene un plugin para eso, [Enigmail](https://www.enigmail.net/index.php/en/) ), recuerda,  la seguridad en internet la hacemos tod@s y solo de nosotr@s depende.

A otra cosa, para este tutorial/manual/guía/articulo se uso la siguiente versión de Thunderbird

![versión](static/img/thunderbird-tor/versión.png  "versión")

## 2.- Configuración Básica.

Bueno, esta es la mas simple de todas, lo único que hacemos es configurar el proxy de Thunderbird para que se redirija al de Tor que corre en segundo plano,
por lo general (si no has modificado nada) seria a la dirección ip *127.0.0.1*, al puerto *9050* y siendo tipo *SOCKS-v5*, en la siguiente imagen se muestra el como 
debe quedar:

![Conf-Básica](static/img/thunderbird-tor/basico.png  "Configuración Básica")

Nos dirigimos a las Preferencias de Thunderbird, seleccionamos la pestaña de *Avanzadas*, dentro podremos ver la sección de *Conexión*, daremos click en el botón
de a lado *Configuración...* y dentro de la nueva ventana que se acaba de abrir seleccionaremos la opción de *Configuración manual de proxy:*, utilizando los
siguientes valores, *Servidor SOCKS: 127.0.0.1*, *Puerto: 9050* y seleccionamos la opción *SOCKSv5*.

Ya con estos simples pasos, nuestro Thunderbird recibirá y enviara sus peticiones por la red Tor, debes tomar en cuenta que algunos proveedores de servicios de correo
(como hotmail) bloquean la cuenta constantemente si ven que las peticiones de ingreso son de IP's diferentes, es decir, Hotmail sabe que eres de México (un decir), pero
si alguien (en este caso tú usando tor) intenta ingresar desde una ip que corresponde a China (otro decir, y recordando que esa es la función de tor, ocultar tu ip real)
pos lo considera como un ataque y pide no se que tantas comprobaciones de que eres tu realmente (creo que te habla a tu teléfono o con un segundo correo, etc).

## 3.- Configuración Avanzada.

Bueno, hasta este punto ya puedes presumir que tu Thunderbird corre bajo la red tor ;) , pero ahora iremos un paso mas aya, si te encuentras en una situación donde
X cosa bloquea el acceso a tu proveedor de correo o no mas pa mayor anonimato (y por ende seguridad) se puede configurar Thunderbird para que utilice las direcciones
[onion](https://es.wikipedia.org/wiki/.onion) de tu proveedor ( claaaaaro si es que este tiene una). Tanto el navegador Firefox como Thunderbird viene por default con
parámetros para no poder usar las paginas onion, en ambos puedes seguir los siguientes pasos para lograr que funcione (solo que en el navegador faltaría agrega uno
que otro detallito mínimo, pero en todo caso se recomienda el uso del [navegador tor](https://www.torproject.org/projects/torbrowser.html.en) si lo que se desea es 
navegar por internet con tor).

Para este ejemplo usaremos el proveedor de servicios de Riseup, que cuenta con [direcciones onion en todos sus servicios](https://riseup.net/en/security/network-security/tor#riseups-tor-hidden-services) (Chat, email, paginas, etc).

Nos dirigimos nuevamente a las preferencias de Thunderbird, en el apartado *Avanzadas*, pestaña *General* y sobre la sección de *Configuración avanzada* daremos
click al botón de *Editor de Configuración...*

![Avanzado-1](static/img/thunderbird-tor/avanzado-1.png "Avanzando-1")

Nos saldrá un cuadro de advertencia, le damos *¡Acepto el riesgo!* ( es buena idea leer lo que dice).

![Avanzado-2](static/img/thunderbird-tor/avanzado-2.png "Avanzando-2")

Realizaremos unas búsquedas para configurar el uso adecuado de las direcciones onion, comenzaremos con buscar **onion** para que en el resultado dando doble click
cambie el valor booleano a *false*

![Avanzado-3](static/img/thunderbird-tor/avanzado-3.png "Avanzando-3")

Ahora buscaremos **network.proxy.s**, para que nos devuelva las configuración relacionadas al proxy, si previamente cambiaste los valores como se describe en el
apartado de configuración básica de este mismo articulo, el resultado de la búsqueda sera igual que el de la imagen, si no lo has hecho y planeas activar el proxy
terminando estas configuraciones, simplemente modifica el apartado de *network.proxy.socks_remote_dns*, cambiando su valor booleano de false a true, dando doble 
click sobre el.

![Avanzado-4](static/img/thunderbird-tor/avanzado-4.png "Avanzando-4")

Por ultimo, debemos buscar **network.proxy.fa** y cambiar el valor que viene por default (en mi caso de 1800) a *5000*.

![Avanzado-5](static/img/thunderbird-tor/avanzado-5.png "Avanzando-5")

Ya con esto tendremos todo lo necesario para poder configurar las direcciones onion de nuestro servidor, para mi cuenta de Riseup, cambien las direcciones como 
se ve en las siguientes imágenes

![IMAP](static/img/thunderbird-tor/imap.png "imap")

![SMTP](static/img/thunderbird-tor/mail-smtp.png "mail-smtp")

Y eso es todo, espero les sirva y/o lo compartan con quienes creen que les puede interesar o servir, gracias por tu tiempo.
