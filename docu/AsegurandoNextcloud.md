Title: Asegurando Nextcloud en tu servidor
Slug: asegurando-nextcloud
Summary: Nextcloud provee una guia de reforzamiento en la seguridad de tu instalacion de Nextcloud, para limitar lo mas posible accesos no autorizados en tu cuenta o las de los usuarios
Date: 2017-08-25 12:00
Category: NextCloud
Tags: basicos, servidores, web, nextcloud, seguridad, 
Author: mazorca
Lang: es
Status: draft

Nuevamente es importante que recuerdes que en este momento esta guia se encuentra en fase de desarrollo, por lo que ten cuidado al usarla en un servidor en produccion. Y que nos basamos en una instalación basada en la documentación de [xihh](./author/xihh.html) en [Instalar y configurar nextcloud](./instalar-nextcloud.html).

Como primer punto deberias de cambiar la ubicacion de los archivos de usuario, que por default esta en `./www/nextcloud/data/` por una ubicacion que no sea accesible desde fuera de internet; y limitar el acceso a la carpeta "./www/nextcloud/.git" la cual estaria disponible si seguiste la guia mencionada. Para hacer esto lo puedes consultar en:  [Actualizar Nextcloud](./recuperacion-de-nextcloud.html).

Una vez que tu servidor de nextcloud esta operativa, puedes acceder a https://xanadu.mazorca.org/index.php/settings/admin con tu cuenta de administrador, lo cual te mostrara una serie de advertencias si es que las configuraciones en tu servidor no son las optimas.

""""INSERTE IMAGEN SOBRE LO ESCRITO ARRIBA******

Estas tres aparecen practicamente en cualquier instalación de NextCloud, asi que vamos a solucionarlas.

###HTTP Strict-Transport-Security

HSTS es una política de seguridad web establecida para evitar ataques que puedan interceptar comunicaciones, cookies, etc. [1]

En Hardening and Security Guidance [2], se explica como hacer la configuración en Apache, pero nosotros estamos usando nginx, por lo que usamos los parametros que se describen en la guia de nginx [3], editamos el archivo /etc/nginx/sites-available/xanadu.mazorca.org para agregar:

```
#Cuando estamos seguro que solo es un dominio por el cual vamos a acceder (xanadu.mazorca.org)
server {
        listen       443 ssl;
        server_name  xanadu.mazorca.org;
add_header Strict-Transport-Security: max-age=15552000;
...

#Si vamos a poder conectar desde varios subdominios (xanadu.mazorca.org, www.xanadu.mazorca.org, otro.xanadu.mazorca.org)
server {
        listen       443 ssl;
        server_name  xanadu.mazorca.org;
add_header Strict-Transport-Security: max-age=15552000; includeSubDomains;
...
```
Nota: en teoria esta configurado, pero sigo sin eliminar el error en la guia

https://schd.io/4zxo nos muestra que si esta configurado el hsts, falta configurar muchas mas cosas

###Memcache para php

Memcache puede mejorar el rendimiento del servidor de NextCloud con almacenamiento en memoria cache de los objetos que son solicitados con mas frecuencia para su recuperacion mas rapida. Se podria omitir esta advertencia, pero nuestro servidor tendria un poco mas de carga en el procesador.

Si es una servidor personal y privado, se recomienda usar APCu

Si es una organizacion mediana y varios usuario Redis
*************NOTA CONFIGURAR MEMCACHE****************

###PHP OPcache no esta configurado apropiadamente

OPcache mejora el rendimiento de PHP almacenando el código de bytes de un script precompilado en la memoria compartida, eliminando así la necesidad de que PHP cargue y analice los script en cada petición. [4]

Para un guia detallada de los valores que vamos a agregar/editar en el php.ini de nuestro servidor consulta este blog [5]

Editamos el archivo:
```
/etc/php5/apache2/php.ini
```

Y cambiamos los datos que nos indica la guia de configuracion de NextCloud para OPCache [6]

```
; Determines if Zend OPCache is enabled
opcache.enable=1

; Determines if Zend OPCache is enabled for the CLI version of PHP
opcache.enable_cli=1

; The OPcache shared memory storage size.
opcache.memory_consumption=128

; The amount of memory for interned strings in Mbytes.
opcache.interned_strings_buffer=8

; The maximum number of keys (scripts) in the OPcache hash table.
; Only numbers between 200 and 100000 are allowed.
opcache.max_accelerated_files=10000

; The maximum percentage of "wasted" memory until a restart is scheduled.
;opcache.max_wasted_percentage=5

; When this directive is enabled, the OPcache appends the current working
; directory to the script key, thus eliminating possible collisions between
; files with the same name (basename). Disabling the directive improves
; performance, but may break existing applications.
;opcache.use_cwd=1

; When disabled, you must reset the OPcache manually or restart the
; webserver for changes to the filesystem to take effect.
;opcache.validate_timestamps=1

; How often (in seconds) to check file timestamps for changes to the shared
; memory storage allocation. ("1" means validate once per second, but only
; once per request. "0" means always validate)
;opcache.revalidate_freq=1

; Enables or disables file search in include_path optimization
;opcache.revalidate_path=0

; If disabled, all PHPDoc comments are dropped from the code to reduce the
; size of the optimized code.
;opcache.save_comments=1

```



'default_language' => 'en',

/**
 * ``true`` allows users to change their display names (on their Personal
 * pages), and ``false`` prevents them from changing their display names.
 */
'allow_user_to_change_display_name' => true,

/**
 * Lifetime of the remember login cookie, which is set when the user clicks
 * the ``remember`` checkbox on the login screen.
 *
 * Defaults to ``60*60*24*15`` seconds (15 days)
 */
'remember_login_cookie_lifetime' => 60*60*24*15,

/**
 * The lifetime of a session after inactivity.
 *
 * Defaults to ``60*60*24`` seconds (24 hours)
 */
'session_lifetime' => 60 * 60 * 24,


/**
 * Enable or disable session keep-alive when a user is logged in to the Web UI.
 * Enabling this sends a "heartbeat" to the server to keep it from timing out.
 * 
 * Defaults to ``true``
 */
'session_keepalive' => true,


/**
 * Whether the bruteforce protection shipped with Nextcloud should be enabled o$
 *
 * Disabling this is discouraged for security reasons.
 * 
 * Defaults to ``true``
 */
'auth.bruteforce.protection.enabled' => true,

/**
 * The directory where the skeleton files are located. These files will be
 * copied to the data directory of new users. Leave empty to not copy any
 * skeleton files.
 *
 * Defaults to ``core/skeleton`` in the Nextcloud directory.
 */
'skeletondirectory' => '/path/to/nextcloud/core/skeleton',

/**
/**
 * Check if Nextcloud is up-to-date and shows a notification if a new version is
 * available.
 * 
 * Defaults to ``true``
 */
'updatechecker' => true,

/**
 * Is Nextcloud connected to the Internet or running in a closed network?
 *
 * Defaults to ``true``
 */
'has_internet_connection' => true,

/**
 * Allows Nextcloud to verify a working WebDAV connection. This is done by
 * attempting to make a WebDAV request from PHP.
 */
'check_for_working_webdav' => true,

/**
 * This is a crucial security check on Apache servers that should always be set
 * to ``true``. This verifies that the ``.htaccess`` file is writable and works.
 * If it is not, then any options controlled by ``.htaccess``, such as large
 * file uploads, will not work. It also runs checks on the ``data/`` directory,
 * which verifies that it can't be accessed directly through the Web server.
 *
 * Defaults to ``true``
 */
'check_for_working_htaccess' => true,

/**
 * In certain environments it is desired to have a read-only configuration file.
 * When this switch is set to ``true`` Nextcloud will not verify whether the
 * configuration is writable. However, it will not be possible to configure
 * all options via the Web interface. Furthermore, when updating Nextcloud
 * it is required to make the configuration file writable again for the update
 * process.
 * 
 * Defaults to ``false``
 */
'config_is_read_only' => false,

* - Image files
 * - Covers of MP3 files
 * - Text documents
 * 
 * Valid values are ``true``, to enable previews, or
 * ``false``, to disable previews
 *
 * Defaults to ``true``
 */
'enable_previews' => true,
/**

[1]https://es.wikipedia.org/wiki/HTTP_Strict_Transport_Security
[2]https://docs.nextcloud.com/server/12/admin_manual/configuration_server/harden_server.html
[3]https://www.nginx.com/blog/http-strict-transport-security-hsts-and-nginx/
[4]http://php.net/manual/es/intro.opcache.php
[5]https://www.scalingphpbook.com/blog/2014/02/14/best-zend-opcache-settings.html
[6]https://docs.nextcloud.com/server/12/admin_manual/configuration_server/server_tuning.html#enable-php-opcache

