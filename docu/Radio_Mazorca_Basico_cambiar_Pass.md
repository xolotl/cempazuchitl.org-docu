Title: Cambiar contraseña en airtime
Summary: Una buena practica es cambiar las contraseñas cada cierto tiempo y en el caso de no haberla generado nosotros, cambiarla al iniciar sesion por primera vez, en airtime es muy sencillo.
Slug:Radio-Mazorca-Cambiar-Pass
Date: 2017-03-25
Category:radio
Tags: basicos, contraseña, acceso, airtime
LANG: es

Cuando accedas a la pantalla de inicio en airtime

![](../static/img/airtimeBasico/Airtime_185.png)

busca esta seccion:

![](../static/img/airtimeBasico/Airtime_218.png)

Abajo del boton que dice **AL AIRE** veras tu nombre de usuario, presiona en el y te llevara a la pantalla donde podras cambiar tu contraseña; ademas de editar tu nombre, correo electronico, y demas datos de contacto

![](../static/img/airtimeBasico/Airtime_239.png)
