Title: Creación de llaves ssh
Date: 2017-02-20 10:20
Category: básicos
Tags: ssh, acceso
Author: mazorca

## Cómo crear un par de llaves ssh

Secure Shell o SSH es un protocolo que nos permite conectarnos de manera segura,
cifrando el canal de comunicación, hacia un servidor remoto,
normalmente a la consola de un servidor tipo unix.

Para conectarnos necesitamos un cliente de ssh y que el destino tenga habilitado el servidor ssh.
El uso normal es ejecutando desde una consola:

	$ ssh usuario@direccion.servidor.destino
	usuario@direccion.servidor.destino's password:


Otra manera de conectar omitiendo la solicitud de contraseña es empleando un juego de llaves.
La comunicación se basa en ofrecer como autenticación en lugar de algo que se (contraseña) algo que tengo (un archivo).
Este par de archivos son una llave pública que comparto con el servidor
(o servidores, puedo usar la misma en varios pero no es recomendable)
y resguardado en mi equipo una llave privada que nadie más debe tener.

Para generar las llaves ejecutamos desde una consola el comando:

	$ ssh-keygen -t rsa -b 4096 -C "Comentario" -f ~/.ssh/NombreLlave

Donde

* **ssh-keygen**		Es el comando que se encargara de generar el juego de llaves y es parte de las utilerías de ssh. (Si no lo tienes	instalado, para los sistemas basados en Debian ''# apt-get install openssh-client'')
* **-t rsa**			Con este parámetro indicamos que use un tipo de cifrado ''rsa''.
* **-b 4096**			Indicamos que el tamaño a usar sea de ''4096bits'', es el valor recomendado actualmente para resistir ataques de fuerza bruta que intenten contra nuestra clave.
* **-C "Comentario"**	Este parámetro es opcional pero recomendado, lo usamos para poner una leyenda por ejemplo "USUARIO en mazorca" y saber a que estamos enlazando.
* **-f nombre**		Con este parámetro indicamos donde se guardara y que nombre tendrán el par de claves

Al ejecutarlo mandara primero el mensaje

	Generating public/private rsa key pair.

Con esto nos avisa que esta trabajando con los parámetros indicados.

Seguido nos pregunta

	Enter passphrase (empty for no passphrase):''

Esta opción añade otra capa de seguridad, además de ofrecer algo que tengo (llave),
preguntará por algo que sé (contraseña).
La contraseña puede ser una frase larga, recuerda los buenos hábitos para generación de passwords.
Para automatizaciones se puede dar Enter y no pedirá contraseña.

	$ ssh-keygen -t rsa -b 4096 -C "USUARIO mazorcaRadio" -f ~/.ssh/USUARIOMazorcaRadio
	Generating public/private rsa key pair.
	Enter passphrase (empty for no passphrase):
	Enter same passphrase again:
	Your identification has been saved in ~/.ssh/USUARIOMazorcaRadio.
	Your public key has been saved in ~/.ssh/USUARIOMazorcaRadio.pub.
	The key fingerprint is:
	SHA256:5LiLVp8+85o6tmtNMfhS5QPLPtC1BlFydQxmvyEmQjQ USUARIO mazorcaRadio
	The key's randomart image is:
	+---[RSA 4096]----+
	|       .Eoo.=o.  |
	|       .o+oo o.  |
	|       +oB..o o  |
	|      o+B.=o . o |
	|      .=S+ .  .  |
	|      o.=        |
	|     ..= o       |
	|    ..+.*.       |
	|   ..o=*+=.      |
	+----[SHA256]-----+



Dependiendo de la capacidad de tu equipo puede tardar varios segundos
pero al final nos indicará que las llaves fueron creadas en la ubicación indicada.

![Generar llaves](../static/img/creacion_llaves_ssh.png)

Se crean dos archivos, uno con el nombre indicado y otro con el nombre y extensión .pub.
Ahora, puedes compartir la llave pública al servidor remoto:

	$ ls -1 /home/USUARIO/.ssh/USUARIOMazorcaRadio*
	/home/USUARIO/.ssh/USUARIOMazorcaRadio
	/home/USUARIO/.ssh/USUARIOMazorcaRadio.pub
