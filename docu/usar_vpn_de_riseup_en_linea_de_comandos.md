Title: Conexión a la vpn de riseup desde linea de comandos.
summary: Cómo conectarse a la vpn de riseup desde linea de comandos.
Slug: conectarse-a-la-vpn-de-riseup-desde-linea-de-comandos
Date: 2017-02-24 00:00
Category: basicos
Tags: vpn, openvpn, riseup
LANG: es

Aunque el método más sencillo para conectarse (y salir) por la vpn que provee riseup es usando la aplicación con interfaZ gráfica [Bitmask](https://riseup.net/en/vpn/how-to/linux), algunxs <del>ñoñxs</del> necesitamos conectarnos desde ordenadores que no tiene interfaz gráfica, así que decidimos documentar cómo hacerlo:

Lo primero que se necesita es activar el acceso a la vpn y se puede hacer entrando a la seccion Passwords después de iniciar sesión en [https://accounts.riseup.net](https://accounts.riseup.net). Lo único que hay que hacer ahí es asignar una contraseña segura (que puede ser creada con ayuda de [esta guía](generador-de-contrasenas.html)).

Después necesitamos instalar estos cuatro programas:

<pre>
~# apt-get install resolvconf openvpn wget curl -y
</pre>

También necesitamos descargar la el certificado público de riseup, que usaremos (pondremos dentro) en el archivo de configuración.

## IMPORTANTE: es necesario verificar la autenticidad del certificado descargado.
<pre>
~# wget https://riseup.net/en/security/network-security/riseup-ca/RiseupCA.pem
</pre>

Podemos guardar el archivo de configuración con el nombre `riseup.openvpn` y poner en él el siguiente contenido:

	client
	dev tun
	proto udp
	keysize 256
	cipher AES-256-CBC
	auth SHA256
	auth-nocache 
	reneg-sec 0
	remote vpn.riseup.net 443
	remote-cert-tls server
	script-security 2
	up /etc/openvpn/update-resolv-conf
	down /etc/openvpn/update-resolv-conf
	<ca>
	RiseupCA.pem
	</ca>
	auth-user-pass .auth

Nótese que en vez de `RiseupCA.pem` (lìnea 10) iría EL CONTENIDO del certificado que descargamos de riseup.

La última lìnea, "auth-user-pass .auth", hace referencia a ".auth", que es el archivo en el que debemos poner las credenciales de acceso da la vpn de riseup.

<pre>
~# nano .auth
</pre>

y su contenido serìa algo como:

<pre>
usuarixRiseup
PassDeVPNdeRiseUp
</pre>

Finalmente podemos conectarnos a la vpn usando el siguiente comando, usando como parámetro el nombre del archivo de configuración que creamos en un paso anterior:

<pre>
~# openvpn riseup.openvpn
</pre>

y comprobar que estamos saliendo por la vpn con:

<pre>
~# curl https://ip.mayfirst.org
</pre>

