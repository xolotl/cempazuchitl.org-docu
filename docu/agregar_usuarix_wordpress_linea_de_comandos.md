Title: Cómo agregar usuarixs a Wordpress desde línea de comandos.
Date: 2017-03-28 15:37
Category: Wordpress
Slug: agragar-usuarix-wordpress-linea-de-comandos
Summary: Cómo agregar una usuaria con privilegios de administración a la base de datos mariadb/mysql de wordpress desde línea de comandos.
Tags: cli, Wordpress, MariaDB, mySQL
Author: todxs

Partiendo de que se tiene acceso al shell de la base de datos, el procedimiento es el siguiente:

Insertar una usuaria llamada 'mazorca' usando la contraseña 'fuckthesystem', y obteniendo el ID con el que fue guardado en la base de datos.

<pre>
INSERT INTO wp_users VALUES (NULL,'mazorca','$P$BEa99/FzxCZrK3QUTtWDFvEebXH8R7/','mazorca','correo@mazorca.org','','2017-03-28 14:39:55','',0,'mazorca');  SELECT LAST_INSERT_ID();
</pre>

Insertar los privilegios de administradora en la tabla de metadatos de usuarixs, reemplazando XXXXXX con el ID obtenido con el comando anterior:

<pre>
INSERT INTO wp_usermeta (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, 'XXXXXX', 'wp_capabilities', 'a:1:{s:13:"administrator";s:1:"1";}');
</pre>

Insertar el nivel de acceso en la misma tabla, reemplazando nuevamente XXXXXX con el ID obtenido en el primer paso:

<pre>
INSERT INTO wp_usermeta (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, 'XXXXXX', 'wp_user_level', '10');
</pre>

Se puede ingresar al sitio usando el nombre de usuarix 'mazorca' y la contraseña 'fuckthesystem' en el formulario de acceso del sitio en cuestión, generelmente accesible en https://sitio.org/wp-login.php.

Inmediatamente después de ingresar por primera vez al sitio, es MUY recomendable cambiar la contraseña asignada y seguir ingresando con la propia. 

Pero es todavía más recomendable jamás olvidar JODER AL SISTEMA.

--happy hacking!
