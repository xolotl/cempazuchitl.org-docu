Title: Presentación de mazorca
SLUG: presentacion
Date: 2017-04-26 12:52
Category: básicos
Tags: html, presentación, git
Author: xihh

# Prerequisitos

Probablemente ya lo tiene, pero es necesario instalar `git`:

```
sudo apt install git
```

# Modificar la presentación

Para colaborar en la presentación de mazorca,
enviar [tu clave pública](/creacion_llaves_ssh.html "Manual para generar un par de llaves para ssh.") a k054:

```
# Descargar la presentación:
git clone git@git.mazorca.org:mazorca-presentacion
# Cambiar al directorio donde se genera:
cd mazorca-presentacion
```

Aquí se puede usar tu editor de texto favorito para editar el archivo `beta.html`
y lo puedes ver con tu navegador usando:

```
# Usamos `&` al final para que puedas seguir usando tu terminal
# los archivos que necesita la página los muestra desde tu computadora.
firefox beta.html &
```

Cada que hagas un cambio en la presentación necesitas apretar `ctrl+R` o `F5`.

Cuando la presentación queda como te gusta:

```
git add beta.html
git commit -m 'Le agregué [esto] a la presentación por ...'
git push
```

Con esto, la presentación se actualizará automáticamente en el servidor
y podrás [verla aquí](https://presentacion.mazorca.org/beta.html "La presentación en fase beta.").

@k054 propone que los cambios se hagan en `beta.html` y cuando estemos conformes,
se pasen a `index.html`, que es lo primero que se muestra en <https://presentacion.mazorca.org/>.

# Previsualizar los cambios interactivamente

El software en el que se basa nuestra presentación
trae la monería de previsualizar la presentación
y recargarla cuando se modifica `index.html`:

```
# Instalar el software necesario para la previsualización
apt get install npm
# Instalar las dependencias de desarrollo web para previsualizar
npm install
# Ejecutar una instancia de node.js y abrir la presentación en tu navegador
# cuando cambies algo, se recargará acá automáticamente.
npm start
```
