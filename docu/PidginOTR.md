Title: Pidgin+OTR
summary: Configuración de pidgin y su complemento otr para tener conversaciones seguras-cifradas punto a punto
slug: configuracion-pidgin-con-plugin-off-the-record-OTR
Date: 2017-03-21 10:20
Category: Mensajeria
Tags: basicos, acceso, pidgin, otr, tor, xmpp
Author: mazorca

#Comunicaciones seguras, con la grabadora apagada y sin saber nuestra ubicación

Pidgin es un software gratuito, de código abierto, que permite conectarte a distintas redes de mensajería instántanea. Entre las redes que conforman su página web se encuentran:

    - AIM
    - Bonjour
    - Gadu-Gadu
    - Google Talk
    - Groupwise
    - ICQ
    - IRC
    - MSN
    - MXit
    - SILC
    - SIMPLE
    - Sametime
    - XMPP
    - Yahoo!
    - Zephyr


Off The Record (OTR). Es un plugin para Pidgin que garantiza comunicaciones autenticadas y seguras entre los usuarios.

Nota: Antes de configurar una cuenta en Pidgin, ya debes tener una cuenta del proveedor de mensajería instantánea que desees utilizar.

###Instalación

Primero vamos a instalar el cliente y el plugin de OTR. Recomendamos la instalación de Tor, para mayor anonimato.

	$sudo apt-get install pidgin pidgin-otr tor

###Antes unos comandos para probar tor

Primero comprobaremos que Tor está instalado, corriendo y puedes salir a través de él:

	$torify curl ip.elbinario.net


* **torify** enviará cualquier petición del comando que le precede, en este caso curl por tor
* **curl** es una herramienta para transferir datos desde o hacia un servidor, en este caso ip.elbinario.net
* **ip.elbinario.net** es una página que te devulve la ip desde la cual se hace una petición, en este caso la dirección que alguien de la red Tor está usando para que salgas a internet

Al ejecutar el comando debería solo aparecer una serie de 4 números separados por un punto. xxx.xxx.xxx.xxx (ninguno será mayor a 255).

Cambiemos ese número, una buena práctica es salir por varias direcciones durante una sesión:

	$sudo service tor restart

**sudo** ejecuta un comando como otro usuario
**service** ejecuta un script de inicio de System V
**tor** script de incio o servicio sobre el que deseamos ejecutar una acción
**restart** la acción que deseamos sobre el script o servicio

Si volvemos a pedir la ip por la que salimos:

	$torify curl ip.elbinario.net

Los números serán distintos. Recuerda, es una buena práctica reiniciar Tor cada cierto tiempo y cambiar la dirección desde la que accedes a internet.

**ahora sí regresemos a Pidgin**

###Configuración de Pidgin (Tor, OTR y cuenta)
 
Ejecuta Pidgin, desde tu escritorio

![](../static/img/pidginotr/pidgin1.png)


Ve al menu _Herramientas_ y luego _Complementos_ (o presiona las teclas CTRL+U) y habilita **Mensajeria Off-the-recordd**

![](../static/img/pidginotr/pidginotractivo.png)

Ve al menu _Cuentas_ y luego _Gestionar cuentas_ (o presiona CTRL+A) y presiona en el botón **Añadir**

Elige el protocolo o servicio que vayas a usar, si no aparece es probable que esté usando xmpp; coloca tu nombre de usuario y dominio, o direccion de tu servicio (por ejemplo riseup.net); adicionalmente puedes poner la contraseña y activar que la recuerde.

![](../static/img/pidginotr/pidginotrcuentabasica.png)

En avanzadas tal y como se ve la imagen (si estamos usando un servicio que tiene un dominio onion, colócalo aqui:

![](../static/img/pidginotr/pidginotrcuentaavanzadas.png)

En pasarela coloca cómo se ve en la imagen. Cuando instalaste Tor, habilitaste un proxy en el puerto 9050 para enviar tráfico a la red.

![](../static/img/pidginotr/pidginotrcuentapasarela.png)

Presiona en el botón aceptar y habrás terminado la configuración de OTR en Pidgin usando la red Tor (si tu servicio de mensajería lo permite).

###Agregando amigos 

Ahora que tu cuenta está configurada es momento de agregar contactos y asegurar las comunicaciones con ellos.

Ve al menú _Amigos y selecciona _Añadir un amigo_ (o presiona CTRL+B)

Recuerda que Pidgin permite gestionar varias cuentas a la vez, por lo que en **Cuenta** debes elegir a qué "cuenta" estás agregando el amigo, colocar el nombre de usuario del amigo, un apodo con el que deseas identificarlo y, en caso de que el servicio lo permita, un mensaje de invitación y el grupo al que deseas que se añada.

![](../static/img/pidginotr/pidginotramigos.png)

Tanto a ti como a tu amigo, les pedirá una autorización para agregarse a a lista de amigos. Si esperas la invitación deberías presionar en **Autorizar*, si no lo esperas y la curiosidad es mucha podrías primero probar primero a enviar un mensaje.

![](../static/img/pidginotr/1-autorizar.png) 

![](../static/img/pidginotr/pidginotramigosautorizar.png) 

Ahora que ambos se encuentran autorizados es posible enviarse mensajes cifrados y Off The Record.

![](../static/img/pidginotr/2.autenticar-llaves.png)

![](../static/img/pidginotr/pidginotrnoverificada.png) 

Pero, como ves, aún falta autentificar que efectivamente nos estamos comunicando con la cuenta de la persona que añadimos.

Para eso, las dos personas deben ir al menú _OTR_ y seleccionar **Autentificar a su colega**

![](../static/img/pidginotr/2-1-autenticar-colega.png) 

Tenemos 3 opciones de autentificación:

    * **Pregunta y respuesta**: Uno de los dos debería poner una pregunta y abajo una respuesta única, el otro debería escribir la respuesta exactamente igual.

    * **Secreto compartido**: Ambos deberían escribir una frase exactamente igual.

    * **Verificación manual de huella de validación**: Mostrará dos cadenas de 40 caracteres divididas en 5 partes cada una. La primera es la propia y la segunda la pretendida de la otra persona.

![](../static/img/pidginotr/3-1-opciones-verificar.png) 

En este ejemplo usaremos la tercera. En una reunión física, te recomendamos revisar que los 40 caracteres de ambas huellas coincidan.

![](../static/img/pidginotr/3-2-verificacion.png) 

![](../static/img/pidginotr/pidginotrautentificandomanualhuella.png) 

Una vez confirmado que ambas huellas coinciden, pueden cambiar **Yo no he** por **Yo he** y presionar en **Autentificar**

![](../static/img/pidginotr/3-3-yo-he-verificado.png)
Ahora puedes que que el estado de la conversacion es privado:

![](../static/img/pidginotr/4-estado-privado.png) 

![](../static/img/pidginotr/pidginotrconversacionprivada.png)

**Disfruta tu privacidad**


[Servicios tor en riseup](https://riseup.net/en/security/network-security/tor#riseups-tor-hidden-services)
[Seguridad en una Caja](https://securityinabox.org/es/guide/pidgin/windows/)
[Pidgin + OTR](https://otr.cypherpunks.ca/)


