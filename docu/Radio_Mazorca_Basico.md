Title: Radio Mazorca Básico
Date: 2017-02-20 10:20
Category: radio
Tags: radio, acceso, airtime
Author: mazorca

### Prerequisitos
Contar con usuario y contraseña

## Airtime
Ingresar a la consola web en la url <http://airtime1d.mazorca.org/> o vía tor <http://g2a3glebu7ugn4uo.onion/>

![](../static/img/airtimeBasico/Airtime_184.png)     ![](../static/img/airtimeBasico/Airtime_186.png)

Al entrar, la primera pantalla que observamos es

![](../static/img/airtimeBasico/Airtime_185.png)


* Se tienen una indicador del audio actual en curso y cual es el siguiente
![](../static/img/airtimeBasico/Airtime_216.png)


* El programa en curso y su tiempo
![](../static/img/airtimeBasico/Airtime_217.png)


* Y claro, el indicador al aire, así como un reloj con la hora local 
![](../static/img/airtimeBasico/Airtime_218.png)

## Temas
* [Subir audio](radio-mazorca-basico-subir.html)
* [Listas de reproducción](radio-mazorca-basico-listas.html)
* [Programación](radio-mazorca-basico-programar.html)
* [Cambiar contraseña](Radio-Mazorca-Cambiar-Pass.html)

## Coordinación
Para comentar horarios, dudas sobre la consola o algún tema relacionado se usa mattermost de mazorca. Se puede entrar a <https://chat.mazorca.org> o por tor  <http://uh7fn4k5wbp75djh.onion>

