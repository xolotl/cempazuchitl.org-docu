Title: Crear cuenta de chat en chat.mazorca.org (mattermost)
Summary: Mattermost es una alternativa para organizar las comunicaciones de un equipo de trabajo, accesible desde computadoras o celulares. Es la opcion que elegimos para coordinar las actividades de mazorca.org
Slug: crear-cuenta-chat-mazorca
Date: 2017-03-25
Category: mattermost
Tags: mattermost, chat, basicos
LANG: es
Author: mazorca

# Creando una cuenta en chat.mazorca.org
* Ingresar al servidor chat.mazorca.org con la liga de invitación 
![](../static/img/chat/creaCuenta038.png)

* Llenar los datos solicitados
![](../static/img/chat/creaCuenta039.png)

* Llegara un correo de confirmación
![](../static/img/chat/creaCuenta040.png)

* Despues de verificar la cuenta
![](../static/img/chat/creaCuenta041.png)

* Llegaras al canal general
![](../static/img/chat/creaCuenta042.png)

* Para ingresar a otros canales, dar click en «Más...» de la sección «CANALES»
![](../static/img/chat/creaCuenta043.png)

* Integrarse a los canales 
![](../static/img/chat/creaCuenta044.png)
