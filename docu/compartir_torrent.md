Title: Compartir con mazorca por Torrent
Slug: compartir-por-torrent
Summary: Cómo compartir un torrent con mazorca
Date: 2017-06-07 12:00
Modified: 2018-01-18 17:01
Category: Torrent
Tags: ssh, torrent, magnet, documentación, cli
Author: ZoRrO
Lang: es

#Intro
Para empezar quisiera agradecer por compartir cualquier tipo de material con mazorca,
ya sea una película, documental, libro(s) o lo que desees compartir con el mundo :), gracias.

Este tutorial se explicara de manera que si deseas hacerlo de manera gráfica se pueda entender pero además se agregan los comandos necesarios si deseas 
realizarlo por medio de la consola.

#Preparativos
Para este tutorial ejemplificaremos usando un archivo de película (V.de.Venganza.DVDRip.Ing.Sub-esp.avi) y sus subtítulos respectivamente (V.de.Venganza.DVDRip.Ing.Sub-esp.srt).
Debemos crear una carpeta en la ubicación en donde se encontraran todos los archivos a compartir, se recomienda que sea con el nombre mas simple, pero descriptivo, 
que se pueda, seguido moveremos los archivos a esta nueva carpeta y nos ubicaremos(entraremos) en ella.

```sh
$ mkdir V.de.Venganza
$ mv V.de.Venganza.DVDRip.Ing.Sub-esp.avi V.de.Venganza.DVDRip.Ing.Sub-esp.srt V.de.Venganza
$ cd V.de.Venganza
```

#Manos a la Obra.
Una vez en la carpeta, crearemos un archivo que se llamara INFO.txt, en donde se encontrara información básica de los archivos a compartir así como un
pequeño mensaje de mazorca al mundo, este archivo es la única petición que se solicita como requisito para compartir cualquier cosa con mazorca.

El archivo INFO.txt constara de el nombre, tamaño(peso), shasum y tipo de archivo(s) seguido del mensaje de mazorca, [aquí](static/torrent/INFO.txt) se puede descargar un ejemplo de este archivo.

Para su creación por medio de la consola los comando son los siguientes(recordemos que nos encontramos en la misma carpeta de los archivos, la que previamente habíamos creado):

```sh
$ echo "Nombre" >> INFO.txt
$ ls V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Peso" >> INFO.txt
$ du -hs V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Shasum 512" >> INFO.txt
$ sha512sum V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Tipo" >> INFO.txt
$ file V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
```

En este caso (y probablemente muchos) como son dos (o mas) archivos los que estamos enviando tendremos que hacer los mismos comandos
para cada uno de ellos separándolos por una linea en blanco entre ellos y al final de todos agregar lo siguiente:

```sh
$ echo " " >> INFO.txt
$ echo "Gracias por compartir con mazorca.org" >> INFO.txt
$ echo "Somos una comunidad de servidor@s basad@s en el apoyo mutuo y la solidaridad en la lucha antisistémica y contra el capitalismo. Nuestro objetivo es simple, ser un punto de partida para lograr la autosuficiencia de las personas y organizaciones que así lo deseen, que ya estén hart@s de vivir bajo la incertidumbre de las cosas y deseen ser los dueños de sí mism@s." >> INFO.txt
$ echo " " >> INFO.txt
$ echo "Links:" >> INFO.txt
$ echo "https://mazorca.org/           http://hfzqzomwdqgnvutt.onion/" >> INFO.txt
$ echo "https://docu.mazorca.org/   http://vm37lyofffctr45l.onion/" >> INFO.txt
$ echo " "  >> INFO.txt
$ echo "Diviértete y ..." >> INFO.txt
$ echo "FUCK THE SYSTEM!" >> INFO.txt
$ echo "<3" >> INFO.txt
```

Es decir para este ejemplo concreto que estamos realizando la lista de comandos completos a utilizar es:

```sh
$ echo "Nombre" >> INFO.txt
$ ls V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Peso" >> INFO.txt
$ du -hs V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Shasum 512" >> INFO.txt
$ sha512sum V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo "Tipo" >> INFO.txt
$ file V.de.Venganza.DVDRip.Ing.Sub-esp.avi >> INFO.txt
$ echo " " >> INFO.txt
$ echo "Nombre" >> INFO.txt
$ ls V.de.Venganza.DVDRip.Ing.Sub-esp.srt >> INFO.txt
$ echo "Peso" >> INFO.txt
$ du -hs V.de.Venganza.DVDRip.Ing.Sub-esp.srt >> INFO.txt
$ echo "Shasum 512" >> INFO.txt
$ sha512sum V.de.Venganza.DVDRip.Ing.Sub-esp.srt >> INFO.txt
$ echo "Tipo" >> INFO.txt
$ file V.de.Venganza.DVDRip.Ing.Sub-esp.srt >> INFO.txt
$ echo " " >> INFO.txt
$ echo "Gracias por compartir con mazorca.org" >> INFO.txt 
$ echo "Somos una comunidad de servidor@s basad@s en el apoyo mutuo y la solidaridad en la lucha antisistémica y contra el capitalismo. Nuestro objetivo es simple, ser un punto de partida para lograr la autosuficiencia de las personas y organizaciones que así lo deseen, que ya estén hart@s de vivir bajo la incertidumbre de las cosas y deseen ser los dueños de sí mism@s." >> INFO.txt
$ echo " " >> INFO.txt
$ echo "Links:" >> INFO.txt
$ echo "https://mazorca.org/           http://hfzqzomwdqgnvutt.onion/" >> INFO.txt
$ echo "https://docu.mazorca.org/   http://vm37lyofffctr45l.onion/" >> INFO.txt
$ echo " " >> INFO.txt[
$ echo "Diviértete y ..." >> INFO.txt
$ echo "FUCK THE SYSTEM!" >> INFO.txt
$ echo "<3" >> INFO.txt
```

Ya al final solo queda crear el torrent de toda la carpeta y para ello podremos seguir el tutorial que se encuentra
aquí mismo en mazorca, mas específicamente los puntos dos y tres de [este](servidor_torrent.md) tutorial.

#Resumen.
Ya al final tendremos 3 archivos en nuestra carpeta que compartiremos:

	-V.de.Venganza.DVDRip.Ing.Sub-esp.avi
	-V.de.Venganza.DVDRip.Ing.Sub-esp.srt
	-INFO.txt

Podríamos corroborarlo con un simple **ls* y  en la medida de lo posible se sugiere además agregar un enlace a alguna referencia del archivo compartido,
si es una película o serie se puede usar los servicios de [TmDB](https://www.themoviedb.org/), si se trata de algún otro tipo de archivo podría referenciarse la fuente
o el origen en si del archivo, de nuevo, es solo un extra, no es necesario; este enlace extra se encontraría después de la información de los archivos antes del
mensaje de *mazorca*(como se muestra en el archivo ejemplo que se puede descargar mas arribita).

Si se siente muy pesado el realizar tantos comando por la consola, estudiando un poco el archivo [ejemplo](static/torrent/INFO.txt) se puede notar que no es tan
complicado y que de manera grafica hay muchas maneras de lograrlo.

**Eso es todo, espero te sirva lo aquí escrito y esperamos pronto compartir contigo. Un agradecimiento especial a *bbabel* que de
una platica con él, salio el 95% de este tutorial ;)**
