# Instalar servidor web LAMP en debian 10 Buster

Nota: Todos los comandos deben ser ejecutados como super usuario o root
  para eso debes utilizar el comando su - (en debian 10 ahora se utiliza "su -" y no "su")

##Actualizar el sistema
  apt update && apt upgrade -y

##Instalar Apache
  apt install apache2 -y

Nota: Comandos basicos para iniciar, detener, reiniciar y ver el estatus del servicio apache2

  * Iniciar el servicio
    systemctl start apache2.service

  * Detener el servicio
    systemctl stop apache2.service

  * Reiniciar el servicio
    systemctl restart apache2.service

  * Recargar el servicio
    systemctl reload apache2.service

  * Ver el estatus del servicio
    systemctl status apache2.service

### Confirmar que funciona ingresando al navegador e ingresando en la barra de direcciones la ip de nuestro servidor, se debe mostrar una pagina de muestra
(imagen1)

##Habilitar la ruta /var/www/html como raiz de nuestra web
Nota: En este caso solo tendre una pagina web en este servidor por lo que utilizare la ruta /var/www/html como unico directorio web

### Dar permisos de propiedad al usuario www-data al directorio con el comando
  chown -R www-data:www-data /var/www/html/

### Habilitar el sitio con el archivo de configuración que nos genera apache2 por default
  a2ensite 000-default.conf

### Reiniciar el servicio apache2
  service apache2 restart

## Instalar MariaDB (Base de datos MySQL libre)
  apt install mariadb-client mariadb-server

### Entrar a la consola de MariaDB para crear bases de datos
  mysql

### Crea base de datos con el nombre ZZZ
  CREATE DATABASE ZZZ;

### Crea usuario XXX con contrasena YYY
  CREATE USER 'XXX'@'localhost' IDENTIFIED BY 'YYY';

### Da priviilegios en base de datos ZZZ a usuario XXX
  GRANT ALL PRIVILEGES ON ZZZ.* to 'XXX'@'localhost';

### Refresca privilegios
  FLUSH PRIVILEGES;

### Salir de la consola MariaDB
  quit

## Instalar Php 7.3 y la libreria para que funcione con apache2
  apt install php libapache2-mod-php -y

###Instalar dependencias para que php 7.3 funcione con MariaDB
    apt install php-mysql -y

##Reiniciar el servidor o en su caso reiniciar el servicio apache2

 * Reinicar servidor
    reboot

 * Reinicar servicio apache2
    systemctl restart apache2.service

Con estos sencillos pasos todo el contenido web que alojemos en la ruta /var/www/html estara disponible en la IP de el servidor.
Por supuesto que faltan muchas cosas para tener un servidor en produccion, como la seguridad en todos sus aspectos (bases de datos, firewall, certificados de seguridad, etc), pero con esto podemos empezar a probar herramientas e ir aprendiendo poco a poco este lindo mundo del software libre y los servidores web.
