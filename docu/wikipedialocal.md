Title: Cómo tener un repositorio de wikipedia local
Slug: wikipedia-local
Summary: El conocimiento debe ser libre y bajo ese esquema fue creada la wikipedia en el #inserte año#, pero hoy en dia esta destinada a solo aquellos que se pueden permitir el acceso a internet; probablemente no lo creas, pero hay mucha gente que no tiene acceso y entonces; el conocimiento ya no es tan libre. O por ejemplo el embargo en Cuba impide que muchas cosas tan comunes entre nosotros no puedan llegar alla.
Date: 2018-06-16 12:00
Category: wikipedia, kiwix
Tags: ssh, LAN, servidores, wikipedia, torrent, kiwix
Author: occidere
Lang: es
Status: draft


Por eso existen varios proyectos[^1] para llevar wikipeadia alla donde la red no alcanza, como las comunidades en donde las empresas no consideran un buen negocio el llevar cables de internet.  y una guia excelente que me ayudo a enteder como hacerlo la puedes consultar aqui [^2]

Pues vamos a partir de debian stretch, recien instalado vamos a la pagina de descargas de wikipedia[^1] vamos a elegir la version que deseemos ocupar:

![./static/img/wikipedia/descargarwiki.png]

En este caso descargaremos la primera, que es la mas completa; tambien es recomendable usar torrent para compartir y no saturar los servidores de wikipedia, o sus espejos. En los dumps de wikipedia[^3] puedes encontrar otras opciones de descarga donde existen dumps completos (incluyendo el sql) para crear una copia que pueda seguir un camino diferente, las paginas estaticas en html, mas archivos zim de otros idiomas o versiones(sin videos o sin imagenes) y un dvd de la CDPedia.

Es importante recalcar que se libera entre uno o dos dumps al mes y que no existe una actualizacion. Si quieres una version mas nueva debes volver a descargar el archivo.

Para el momento de hacer esta documentacion tengo el archivo kiwix-0.9+wikipedia_es_all_novid_2018-04.zip el cual trae los archivos zim y el software wikix[^4] para visualizarlos. Con ejecutables para linux, winbugs y macs.

Vamos a descomprimir todo en el directorio /opt con la intencion de dejarlo como un servicio,

    $sudo mkdir /opt/wikis/
    $sudo chown -R occidere:occidere /opt/wikis
    $unzip kiwix-0.9+wikipedia_es_all_novid_2018-04.zip -d ~/

Con eso debemos obtener lo siguiente:

   ```$ ls -lh ~/

    total 75M
    drwxr-xr-x 8 occidere occidere 4.0K jun 16 14:50 .
    drwxr-xr-x 3 root     root     4.0K jun 10 11:48 ..
    drwxr-xr-x 3 occidere occidere 4.0K dic  1  2010 autorun
    -rw-r--r-- 1 occidere occidere  197 abr 18 09:26 autorun.inf
    drwxr-xr-x 5 occidere occidere 4.0K abr 17 04:46 data
    -rw-r--r-- 1 occidere occidere  19K may 24 00:58 dumpstatus.json
    drwxr-xr-x 2 occidere occidere 4.0K oct 30  2014 install
    drwxr-xr-x 7 occidere occidere 4.0K oct 30  2014 kiwix
    -rw-r--r-- 1 occidere occidere 2.0M oct 29  2014 kiwix-0.9-src.tar.xz
    drwxr-xr-x 3 occidere occidere 4.0K oct 29  2014 Kiwix.app
    -rw-r--r-- 1 occidere occidere 756K oct 30  2014 kiwix.exe
    -rw-r--r-- 1 occidere occidere  72M abr 17 04:45 kiwix-linux.tar.bz2
    ```

Movemos el directorio data/ a su nueva ubicacion:

    $mv data/ /opt/wikis

Vamos a descomprimir kiwi para tener el ejecutable del servidor:

    $tar -C /opt/wikis/ -xvjf kiwix-linux.tar.bz2

Ahora ya tenemos el ejecutable y el contenido de wikipedia, pasemos a crear un servicio para que se ejecute siempre al inicio.

    $sudo nano /etc/systemd/system/kiwix.service

Y le agregamos:

    ```[Unit]
    Description=Servicio local de wikipedia
    After=network.target
 
    [Service]
    Type=forking
    ExecStart=/opt/wikis/kiwix-linux/bin/kiwix-serve --daemon --library /opt/wikis/data/library/wikipedia_es_all_novid_2018-04.zim.xml 
 
    [Install]
    WantedBy=multi-user.target
    ```
Lo que hay que destacar de aqui es:
 **After=network.target** Se cargara despues de que haya iniciado la red
 **ExecStart** Es la ruta en la que se encuentre el ejecutable del servidor, se agrego como demonio *daemon*, en el caso del archivo que descargamos trae un xml con la libreria de los archivos zim; que a futuro lo podremos usar para agregar otras wikis y agregamos el puerto 8081 *--port 8081* por que vamos a poner un proxy https con nginx*

Habilitamos el servicio y lo cargamos:

    sudo systemctl enable kiwix.service
    sudo systemctl start kiwix.service

Ahora podemos cargar la pagina usando la IP de nuestra maquina y el puerto 8081

![./static/img/wikipedia/cargawiki.png]

Y cargarla para consultarla:

![./static/img/wikipedia/consultawiki.png]

**Library** Te regresara a elegir la wiki que desees consultar.
**Home** Te llevara al inicio de la wiki que tienes elegida actualmente.
**Random** Te cargara un articulo al azar.
**Search** Para buscar contenido.



[^1]: https://es.wikipedia.org/wiki/Wikipedia:Descargas
[^2]: https://www.sysadminsdecuba.com/2018/01/tips-kiwix-como-servicio-en-ubuntu-server-16-04-para-tu-lan/
[^3]: https://dumps.wikimedia.org/

