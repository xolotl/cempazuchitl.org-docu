Title: Cómo instalar una instancia de ethercalc
Date: 2017-02-24 00:00
Category: webapps
Tags: ethercalc, debian, jessie, npm, nodejs
LANG: es

Agregar repos de nodejs

	echo -e "deb https://deb.nodesource.com/node_6.x jessie main \r\ndeb-src https://deb.nodesource.com/node_6.x jessie main"| sudo tee /etc/apt/sources.list.d/nodesource.list

Agregar llave del repositorio

	curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -	

Actualizar lista de paquetes en los repositorios

	sudo apt-get update

Instalar paquetes necesarios

	sudo apt-get install nodejs redis-server -y

Clonar repo de ethercalc

	git clone https://github.com/audreyt/ethercalc.git

Ir al directorio de ethercalc

	cd ethercalc  

Instalar dependencias usando npm

	npm i zappajs redis j csv-parse redis socialcalc nodemailer node mailer mailer xoauth2 uuid-pure

Ejecutar ethercalc en el puerto 8000

	bin/ethercalc 
