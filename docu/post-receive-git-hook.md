Title: Post-receive Git hook
summary: Ordenar a git que realice una acción después de recibir un push
slug: ordenar-a-git-que-realice-accion-despues-de-recibir-push
Date: 2018-02-21 22:30
Category: Git
Tags: basicos, git, hooks, push
Author: mazorca
status: draft

#Git es un sistema de control de versiones; hooks son reacciones a comandos de git

Los sistemas de control de versiones fueron creados para gestionar código (aunque en realidad puede ser cualquier texto), de manera que sea posible consultar los cambios y/o estado del proyecto en cualquier momento, sin la necesidad de hacer una copia distinta de cada cambio realizado.
Además, muchos sistemas de control de versiones facilitan la colaboración entre desarrolladorxs, al implementar métodos que permiten que varias personas trabajen en un mismo proyecto o archivo, y después combinen y/o descarten las modificaciones hechas por cada contrbuidorx.

###
