TITLE: generador de contraseñas
DATE: 2017-02-24 00:00
CATEGORY: basicos
TAGS: bash, contraseña, basicos
SLUG: generar-contrasenas-seguras-desde-linea-de-comandos
SUMMARY: generar contraseñas seguras desde línea de comandos
LANG: es

Con el siguiente procedimiento podemos tener un generador de contraseñas disponible en nuestro shell todo el tiempo.

Primero necesitamos crear/editar un script y que puede ser invocado desde el shell:

<pre>
~$ nano .genpasswd
</pre>

y agregar el siguiente contenido:

<pre>
genpasswd() { 
	local l=$1
	[ "$l" == "" ] && l=20
	tr -dc A-Za-z0-9+-_]*[}{^\&\#@.,_ < /dev/urandom | head -c ${l} | xargs 
}
</pre>

Después debemos agregar una linea que incluya ese script al final de nuestro archivo de configuración de bash:

<pre>
~$ echo "source .genpasswd ">> ~/.bashrc
</pre>

Finalmente, debemos recarga el archvo de configuración de bash (o abrir un shell nuevo para que cargue la configuración):

<pre>
~$ source ~/.bashrc
</pre>

Ahora podmeos invocar la generación de una contraseña segura con:

<pre>
~$ genpasswd
</pre>

El script acepta un parámetro numérico para determinar la longitud, asi que en caso de querer una contraseña de 50 caracteres es posible hacer:

<pre>
~$ genpasswd 50
</pre>
