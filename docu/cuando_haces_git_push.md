Title: Acciones para cuando subes cambios con `git push`.
Date: 2017-03-26 15:37
Category: git
Tags: git, hooks
Slug: cuando-haces-git-push
Summary: Cómo ejecutar acciones cuando subes los cambios con git.
Author: xihh

Git permite ejecutar acciones cuando pasa algo específico en el repositorio.
Estas acciones en inglés se llaman "hook", así que aquí les diremos ganchos de git.

Mazorca usa [gitolite](http://gitolite.com/gitolite/index.html "Revisa que quien se conecta tenga permisos para usar el repo.").
Este programa configura un usuario para verificar con la clave pública quién se conecta y qué información puede usar.
Para información más completa, [seguir este manual](http://gitolite.com/gitolite/install.html "Manual de instalación de gitolite.").

Gitolite se instala en un usuario y es necesario tener acceso a este usuario para configurar los ganchos.

```
# Conectarse al servidor donde vive gitolite:
ssh [tu-usuario]@[servidor]
# Utilizar el usuario de gitolite:
sudo -u [usuario-de-gitolite] -H -s
# Cambiarse al hogar ($HOME) del usuario de gitolite
# donde generalmente viven los repositorios
cd ~/repositories/
```

Allí se pueden ver los repositorios "puros" (porque no contienen directorio de trabajo) que viven en el servidor.
Cada uno tiene los siguientes directorios:

```
$ ls -F [repo]
branches/      config         FETCH_HEAD     hooks/         info/          objects/       packed-refs
COMMIT_EDITMSG description    HEAD           index          logs/          ORIG_HEAD      refs/
```

De aquí nos interesa el directorio hooks,
donde generalmente hay guiones (scripts) de ejemplo
para cada etapa donde se puede agregar un gancho:

```
$ ls ~/repositories/[repo]/hooks
applypatch-msg.sample     pre-applypatch.sample     pre-push.sample           update.sample
commit-msg.sample         pre-commit.sample         pre-rebase.sample
post-update.sample        prepare-commit-msg.sample pre-receive.sample
```

# Preparar el servidor remoto

Si tu repositorio de git vive en un lugar distinto donde se va a ejecutar la acción
conviene [generar un par de llaves ssh](/creacion_llaves_ssh.html "Manual para generar un par de llaves para ssh.")
para acceder al servidor sin necesidad de usar una contraseña.

```
ssh-keygen -t rsa -b 4096 -C 'propósito de la llave' -f ~/.ssh/[nombre-de-la-llave]
ssh-copy-id -i ~/.ssh/[nombre-de-la-llave] usuario@servidor-remoto
```

# Configurar el gancho

El gancho que se ejecuta cuando alguien sube sus cambios usando `git push` es `hooks/post-receive`.
Allí tenemos que guardar las instrucciones para actualizar el sitio web,
compilar el código y ejecutar las pruebas...

```
#!/bin/sh
# Ejemplo para actualizar las presentaciones de mazorca
# este es `presentacion-mazorca/hooks/post-receive` desde gitolite
# se le dieron permisos de ejecución con `chmod +x`
git push git@presentacion:/var/www/html master:gitolite
ssh git@presentacion 'cd /var/www/html; git merge gitolite; git reset --hard;'
```

Se configuró el usuario de git para usar un par de llaves criptográficas en vez de usar contraseña
y subir los cambios a presentación.

Con estos dos guiones, cuando gitolite lo recibe,
empuja los cambios al servidor de la presentación
y cuando "presentación" lo recibe,
lo actualiza en la página web.
