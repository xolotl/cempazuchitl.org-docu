Title: Generar, instalar y configurar certificados tls/ssl con let's encrypt en debian jessie y nginx.
Date: 2017-02-24 00:00
Category: nginx
Tags: let's encrypt, nginx, debian, jessie, ssl, tls
Author: mazorca
LANG: es

Instalar soporte para https en repositorios

	 sudo apt-get install apt-transport-https

Agregar repos de backports en jessie.

	echo -e "deb https://ftp.debian.org debian jessie-backports main \r\ndeb-src https://ftp.debian.org debian jessie-backports main"| sudo tee /etc/apt/sources.list.d/jessie-backports.list

Actualizar lista de paquetes en los repositorios

	sudo apt-get update

Instalar nginx

	sudo apt-get install  nginx -y

Declarar el nombre del dominio o subdominio

	DOMINIO=acab.mazorca.org

Declarar el correo que recibirá avisos de expiración de certificado

	 EMAIL="correo@electronico.org"


Crear archivo de configuracion para el dominio o subdominio

	nano /etc/nginx/sites-available/$DOMINIO

Agregar la siguiente declaración al archivo de configuración del dominio. Reemplaza '$DOMINIO' por el dominio que usarás. 

<pre>
server {
        listen 80;
        server_name $DOMINIO;
#       rewrite ^/(.*) https://$DOMINIO/$1;
        root /var/www/html/$DOMINIO;
        index index.html index.htm index.php;

        access_log   /var/log/nginx/$DOMINIO.access.log combined;
        error_log   /var/log/nginx/$DOMINIO.error.log warn;
}
</pre>

habilitar el dominio

	ln -s /etc/nginx/sites-available/$DOMINIO /etc/nginx/sites-enabled/$DOMINIO

Crear una directorio para el dominio

	mkdir /var/www/html/$DOMINIO

Crear un index para el dominio

	echo "HolaMundo // FuckYOU capitalism!!" >> /var/www/html/$DOMINIO/index.html

Comprueba el que la configuración de nginx funcione correctamente.

	sudo nginx -t

Si el paso anterior responde que funciona adecuadamente, reinicia nginx

	 sudo systemctl restart nginx

Verificar si el dominio está resolviendo adecuadamente en http:

	curl http://$DOMINIO

Instalar certbot

	sudo apt-get install certbot -t jessie-backports -y

Generar certificados

	certbot certonly --webroot -w /var/www/html/$DOMINIO -d "$DOMINIO" -m "$EMAIL"

Agragar declaracion de servicio bajo tls/ssl a la configuración nginx del domino o subdomino.

Agregar lìneas de tls/ssl al archivo de configuracion del dominio o subdominio creado en anteriormente

	nano /etc/nginx/sites-available/$DOMINIO

<pre>
server {
        listen       443 ssl;
        server_name  $DOMINIO;

        access_log  /var/log/nginx/$DOMINIO.access.log;
        error_log   /var/log/nginx/$DOMINIO.error.log;

        ssl                  on;
        ssl_certificate	/etc/letsencrypt/live/$DOMINIO/fullchain.pem;
        ssl_certificate_key	/etc/letsencrypt/live/$DOMINIO/privkey.pem;

        ssl_session_timeout  5m;

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS";

        root /var/www/html/$DOMINIO;
        index index.html index.htm index.php;

	# let'sencrypt
	location /.well-known {
		root /var/www/html/;
	}

        access_log   /var/log/nginx/$DOMINIO.access.log combined;
        error_log   /var/log/nginx/$DOMINIO.error.log warn;

}
</pre>

verificar si el dominio está resolviendo adecuadamente en https:

	curl https://$DOMINIO

Si la salida del comando anterior fuera: 

	"HolaMundo // FuckYOU capitalism!!"

podemos descomentar la siguiente lìnea del archivo de configuración del dominio o subdominio para que todas las llamadas de http se redirijan autómagicamente a https:

<pre>
nano /etc/nginx/sites-available/$DOMINIO
</pre>

La cuarta lìnea deberìa quedar así:

<pre>
	rewrite ^/(.*) https://$DOMINIO/$1;
</pre>

Si se quiere que ese redireccionamiento sea permanente (el redireccionamiento se guarda en el cache del navegador web que visite el sitio) tambièn puede agragar el paràmetro 'permanent' y la línea quedarìa asì

<pre>
	rewrite ^/(.*) https://$DOMINIO/$1 permanent;
</pre>

Aunque vale aclarar que eso es recomendable sólo cuando ya se han hecho todas las pruebas de que el dominio resuelve adecuadamente.


Finalmente, vale escribir que a diferencia de los certificados expedidos por autoridades comerciales que tienen "caducidad" de un año, los certificados de let's encrypt son válidos por 3 meses, y hay que renovarlos cotidianamente para prevenir que quienes visitan nuestros sitios se encuentren con advertencias de seguridad.

Y es para automatizar el proceso, que además de la sección que pusimos bajo el comentario "#let's encrypt" en la configuración de nginx, podemos ejecutar el siguiente comando, que agrega una tarea al cronjob que intenta renovar el certificado todos los días a las 0 y a las 12 horas, pero que sólo funcionaría cuando falten menos de 7 días para que el certificado vigente venza, de modo que no se renueva diariamente, sino sólo cuando la fecha de expiración está próxima a ocurrir.

<pre>
	echo "* 0,12 * * * certbot renew" >> /etc/crontab
</pre>


-- happy hacking
