#!/bin/bash
##Script de respaldo y envio mediante ssh a otro equipo
##Creative Commons
##By xolotl@sursiendo.org
###########################################################

#Sentencias
User_NC=
Pass_NC=
Server_NC=
SourceDir=
BackUpFile=
SSHUser=
SSHPass=
SSHServerPhat=

#Sincronización de Nextcloud
echo "Comenzando sincronización"
nextcloudcmd -u $User_NC -p $Pass_NC $SourceDir $Server_NC
echo "La sincronización fue exitosa"

#Comprimir carpeta a un archivo TAR
echo "Comprimiendo archivos"
tar -zcvf  $BackUpFile $SourceDir
echo "La compresión fue exitosa"

#Enviar por ssh
echo "Enviando archivos a servidor remoto"
sshpass -p $SSHPass scp $BackUpFile $SSHUser@$SSHServerPhat
echo "Los archivos han sido enviados satisfactoriamente"
